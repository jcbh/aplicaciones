<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="com.tecnificados.gestime.bean.EventoGC"%>
<%@ page import="com.tecnificados.gestime.bean.Proyecto"%>
<%@ page import="com.tecnificados.gestime.bean.Imputacion"%>
<%@ page import="java.text.SimpleDateFormat"%>



<%

SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

ArrayList<Proyecto> proyectos=new ArrayList<Proyecto>();

Map <String, String> mapaProyectoHoras = new HashMap<String, String>();

Map <String, ArrayList<Imputacion>> mapaProyectoImputacion= new HashMap<String, ArrayList<Imputacion>>();

String pId="";
String pNombre="";

String errorBD="";
String errorParseo="";

boolean error=false;

if (request.getParameter("errorBD")!=null){
	errorBD=request.getParameter("errorBD");
	error=true;
}

if (request.getParameter("errorParseo")!=null){
	errorParseo=request.getParameter("errorParseo");
	error=true;
}


if (request.getSession(true).getAttribute("proyectos")!=null)
	proyectos=(ArrayList<Proyecto>)request.getSession(true).getAttribute("proyectos");

if (request.getSession(true).getAttribute("mapa")!=null)
	mapaProyectoHoras=(Map <String, String> )request.getSession(true).getAttribute("mapa");

if (request.getSession(true).getAttribute("mapaProyectoImputacion")!=null)
	mapaProyectoImputacion=(Map <String, ArrayList<Imputacion>> )request.getSession(true).getAttribute("mapaProyectoImputacion");

if (request.getParameter("pId")!=null){
	pId=(String)request.getParameter("pId");
	System.out.println(pId);
}




%>



<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">



</head>

<body>



	<jsp:include page="menu.jsp" ></jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h2>Imputaciones en tus Proyectos</h2>			
		</div>
	</div>

	<div class="container">
		<div class="row separador">				
				<div class="col-md-6">					
					<h3><span class="label label-primary">Tus Proyectos</span></h3>
				</div>
				<div class="col-md-6 hide" id="tituloFiltros">					
					<h3><span class="label label-primary">Filtros</span></h3>
				</div>
		</div>
	
		<div class="row">	
			
			<div class="col-md-6">
					
				<div class="row">
					<div class="col-md-3"></div>
						<div class="col-md-9">
						 	<div class="list-group">			
							<%for (Proyecto p:proyectos){ 
								
								if ((!pId.equals(""))&&(pId.equals(p.getId()))){
									pNombre=p.getNombre();
								}
							
								if (mapaProyectoHoras.get(p.getId())!=null){%>
								
									<% if (!mapaProyectoHoras.get(p.getId()).equals("0.0")){%>
							 		<a href="javascript:seleccionaProyecto('<%=p.getId()%>','<%=p.getNombre()%>')" class="list-group-item">
							 			&nbsp;<%=p.getNombre()%> <span class="badge pull-right"><%=mapaProyectoHoras.get(p.getId())%> / <%=p.getHorasI()%></span>
							 		</a>
							 		<% }else {%>
							 		<a href="#" class="list-group-item">
							 			&nbsp;<%=p.getNombre()%> <span class="badge pull-right"><%=mapaProyectoHoras.get(p.getId())%> / <%=p.getHorasI()%></span></a>
							 		<%} %>
								<%}else{ %>
									<a href="#" class="list-group-item">
										&nbsp;<%=p.getNombre()%> <span class="badge pull-right"><%=p.getHorasI()%></span></a>	  	
								<%}		  	
							}%>	
						</div>
						</div>	
						
				</div>	
				
							
		
				</div>
				<div class="col-md-6 hide" id="capaFiltros">
				
					<div class="row">						
						<div class="col-md-offset-1 col-md-5"><h4><span class="label label-primary">Estado</span></h4></div>
					</div>
					<div class="row">
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-8">
							<select class="form-control" id="estadosFiltro" onchange="cambiaEstado(this.value)">
							  <option value="">Todos</option>	
							  <option value="0">Pendientes</option>		
							  <option value="1">Aprobadas</option>
							  <option value="-1">Rechazadas</option>							  						  
							</select>	
						</div>						  
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">						
						<div class="col-md-offset-1 col-md-4"><h4><span class="label label-primary">Fecha Inicial</span></h4></div>
						<div class="col-md-offset-1 col-md-4"><h4><span class="label label-primary">Fecha Final</span></h4></div>
					</div>
					<div class="row">
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-3">						
							<input class="form-control" type="text" name="dia" id="fechaInicio" maxlength="10" placeholder="dd/mm/aaaa">									
						</div>
						<div class="col-md-1">
							<a href="javascript:borraFechaInicio()"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
						<div class="col-md-1">&nbsp;</div>
						<div class="col-md-3">						
							<input class="form-control" type="text" name="dia" id="fechaFin" maxlength="10" placeholder="dd/mm/aaaa">										
						</div>
						<div class="col-md-1">
							<a href="javascript:borraFechaFin()"><span class="glyphicon glyphicon-remove"></span></a>
						</div>
					</div>
					<div class="row">&nbsp;</div>
					<div class="row">						
						<div class="col-md-offset-1 col-md-4"><h4><span class="label label-primary">Usuarios</span></h4></div>						
					</div>
					<div class="row">
						<div class="col-md-2">&nbsp;</div>
						<div class="col-md-8">								
							<select class="form-control" id="usuariosFiltro" onchange="cambiaUsuario(this.value)">
							  <option value="">Todos</option>							  
							</select>
																
						</div>
					</div>
				
  
  			
			</div>
				
				
				
			<div class="row separador">				
				<div class="col-md-12">					
					<h3><span class="label label-primary hide" id="tituloProyecto">Tus Proyectos</span></h3>
				</div>
			</div>


		<%if (errorBD.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Se ha producido un error de base de datos. Por favor contacte con el Administrador.</div>
		</div>
		<%} %>

		
		<%if (errorParseo.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Los datos no están llegado correctamente. Por favor contacte con el Administrador.</div>
		<%} %>
	
			<form method="post" action="imputPro" name="imputacionesForm" id="imputacionesForm">
			<input type="hidden" name="imputacionId" id="imputacionId" value="">
			<input type="hidden" name="accion" id="accion" value="">
			<%
			Set<String> proyectosIds=mapaProyectoImputacion.keySet();
			
			for (String proyectoId:proyectosIds)
			{				 
				ArrayList<Imputacion> imputaciones=mapaProyectoImputacion.get(proyectoId);
				%>				  
				    
				  <table class="table" id="tablaIP<%=proyectoId %>">			    
			        <thead>
			          <tr>
			            <th>Usuario</th>
			            <th>Horas</th>
			            <th>Fecha</th>
			            <th>Descripción</th>
			            <th>Estado</th>
			          </tr>
			        </thead>
			        <tbody>
				
				<%
				for (Imputacion imputacion:imputaciones)
				{
				%>
				   <tr id="fila_Proyecto_<%=imputacion.getProyecto()%>_Estado_<%=imputacion.getEstado()%>_Fecha_<%=formatoFecha.format(imputacion.getFecha())%>_Usuario_<%=imputacion.getUsuario()%>">
		            <td><span id="usuario<%=imputacion.getProyecto()%>"><%=imputacion.getUsuario()%></span></td>
		            <td><%=imputacion.getHoras()%></td>
		            <td><%=formatoFecha.format(imputacion.getFecha())%></td>
		            <td><%=imputacion.getDescripcion()%></td>
		            
		            <%
		            if (imputacion.getEstado()==0)
		            {
		            %>
		            <td>
		            	<span class="glyphicon glyphicon-warning-sign"></span>&nbsp;Pendiente
		            	<a href="javascript:aprobar('<%=imputacion.getId()%>')"><span class="glyphicon glyphicon-warning-ok"></span>&nbsp;Aprobar</a>
		            	<a href="javascript:rechazar('<%=imputacion.getId()%>')""><span class="glyphicon glyphicon-warning-remove"></span>&nbsp;Rechazar</a>
		            </td>
		            <%}else if (imputacion.getEstado()==1){%>
		            <td>
		            	<span class="glyphicon glyphicon-ok"></span>&nbsp;Aprobada		            	
		            	<a href="javascript:rechazar('<%=imputacion.getId()%>')""><span class="glyphicon glyphicon-warning-remove"></span>&nbsp;Rechazar</a>
		            </td>
		            <%}else if (imputacion.getEstado()==-1){%>
		            <td>
		            	<span class="glyphicon glyphicon-remove"></span>&nbsp;Rechazada
		            	<a href="javascript:aprobar('<%=imputacion.getId()%>')"><span class="glyphicon glyphicon-warning-ok"></span>&nbsp;Aprobar</a>		            	
		            </td>
		            <%} %>
		          </tr>
				<% 	
				}
				
				%>
				    </tbody>
		      </table>
		      
				<%}%>
			  
			</form>
			  
				
				
				
			
		
	
	
		
		<hr>
		</div>
	  <jsp:include page="pie.jsp" ></jsp:include>



		<!-- Bootstrap core JavaScript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>		 
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>		
		<script src="/js/bootstrap.min.js"></script>
</body>
</html>
<script>
var estado="";
var fechaInicio="";
var fechaFin="";
var usuario="";
var proyecto="";

$(function() {
	
	$("[id^=tablaIP]").each(function () {
	    $(this).addClass("hide");		    
	});
	
	seleccionaProyecto('<%=pId%>','<%=pNombre%>');	

	
	$.datepicker.regional['es'] = {
	        closeText: 'Cerrar',
	        prevText: '<Ant',
	        nextText: 'Sig>',
	        currentText: 'Hoy',
	        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	        weekHeader: 'Sm',
	        dateFormat: 'dd/mm/yy',
	        firstDay: 1,
	        isRTL: false,
	        showMonthAfterYear: false,
	        yearSuffix: ''
	    };
	    $.datepicker.setDefaults($.datepicker.regional['es']);
	
	
	
	

	
	    $("#fechaInicio, #fechaFin").datepicker({dateFormat: 'dd/mm/yy'});
		$("#fechaInicio, #fechaFin").val();		
	
		
		 $("#fechaInicio").change(function(){
			 cambiaFechaInicio($("#fechaInicio").val());	            
	     });
		 
		 $("#fechaFin").change(function(){
			 cambiaFechaFin($("#fechaFin").val());	            
	     });
		  
	
	
});

function cambiaEstado(valor)
{
	estado=new String(valor);
	filtra();
}

function cambiaFechaInicio(valor)
{
	fechaInicio=valor;
	filtra();
}

function cambiaFechaFin(valor)
{
	fechaFin=valor;
	filtra();
}

function cambiaUsuario(valor)
{
	$("#usuariosLiIni").html("<a href=''>"+valor+"</a>");
	usuario=valor;
	
	filtra();
}

function borraFechaInicio()
{
	$("#fechaInicio").val("");	
	cambiaFechaInicio('');
}

function borraFechaFin()
{
	$("#fechaFin").val("");	
	cambiaFechaFin('');
}

function filtra()
{
	/*
	alert(estado);
	alert(fechaInicio);
	alert(fechaFin);	
	alert(usuario);
	*/
	
	/*
	fila_Proyecto_P000000003_Estado_1_Fecha_13/05/2014_Usuario_jcballesteroshermida
	*/
	
	
	
	//selecciono todas filas del proyecto y las hago visibles
	$("[id^=fila_Proyecto_"+proyecto+"]").each(function () {
		 $(this).removeClass('hide');
	
	});
	
	
	//selecciono todas filas del proyecto y compruebo el filtro
	
	//alert("[id^=fila_Proyecto_"+proyecto+"]")
	
	$("[id^=fila_Proyecto_"+proyecto+"]").each(function () {
		
		console.log("filtro estado:"+estado)
		
		var currentId = $(this).attr('id');
		//console.log(currentId);
		estadoFila=currentId.substring(currentId.indexOf("Estado_")+("Estado_".length),currentId.indexOf("_Fecha"));
		//console.log("filtro fila:"+estadoFila)		
		fechaFila=currentId.substring(currentId.indexOf("Fecha_")+("Fecha_".length),currentId.indexOf("_Usuario"));
		//console.log(fechaFila)
		usuarioFila=currentId.substring(currentId.indexOf("Usuario_")+("Usuario_".length),currentId.length);
		//console.log(usuarioFila)
		
		var filtroOK=false;
		
		/*
		alert(estado+" "+estadoFila)
		alert(estado!=estadoFila)
		*/
		
		
		if (estado!="")
		{
		
			if (estado==estadoFila)
			{
				filtroOK=true;				
			}else{
				filtroOK=false;
			}
		}else{
			filtroOK=true;
		}
		
		if (filtroOK)
		{
			if (usuario!="")
			{
				if (usuario==usuarioFila)
				{
					filtroOK=true;
				}
				else
				{
					filtroOK=false;
				}
			}			
		
		}
		
		if (filtroOK)
		{
			if (fechaInicio!="")
			{
				var dateFechaInicio=cadenaToDate(fechaInicio);
				var dateFechaFila=cadenaToDate(fechaFila);
				
				if (dateFechaInicio.getTime()<=dateFechaFila.getTime())
					filtroOK=true;
				else
					filtroOK=false;
					
			}
			
		}
		
		if (filtroOK)
		{
			if (fechaFin!="")
			{
				var dateFechaFin=cadenaToDate(fechaFin);
				var dateFechaFila=cadenaToDate(fechaFila);
				
				if (dateFechaFin.getTime()>=dateFechaFila.getTime())
					filtroOK=true;
				else
					filtroOK=false;
					
			}
			
		}
		
		
		
		if (filtroOK==false){
			 $(this).addClass('hide');
		
		}
		
		
		
	});
}

function seleccionaProyecto(pId,pNombre)
{	
	
	
	
	//alert(pId)
	if (pId=="")
		return;
	
	
	proyecto=pId;
	
	$("#tituloFiltros").removeClass("hide");
	$("#capaFiltros").removeClass("hide");
	
	
	$("#tituloProyecto").text(pNombre);
	$("#tituloProyecto").removeClass("hide");
	
	
	$("[id^=tablaIP]").each(function () {
	    $(this).addClass("hide");		    
	});
	
	//obtenemos los usuarios del proyecto
	var arrayUsuarios=new Array();
	$("[id^=usuario"+pId+"]").each(function () {
	   if (arrayUsuarios.indexOf($(this).text())<0)
		   arrayUsuarios.push($(this).text());
	});
	
	//alert(arrayUsuarios);
	
	$("#usuariosFiltro").empty();
	$("#usuariosFiltro").append("<option value=''>Todos</option>");	
	
	
	for (var i=0;i<arrayUsuarios.length;i++)
		{
			$("#usuariosFiltro").append("<option value='"+arrayUsuarios[i]+"'>"+arrayUsuarios[i]+"</option>");	
		}
	
	
	$("#tablaIP"+pId).removeClass("hide");
}

function aprobar(pId)
{
	
	$("#imputacionId").val(pId);
	$("#accion").val('aprobar');
	$("#imputacionesForm").submit();
}

function rechazar(pId)
{
	$("#imputacionId").val(pId);
	$("#accion").val('rechazar');
	$("#imputacionesForm").submit();
}

// dd/mm/aaa to date
function cadenaToDate(cadenaFecha)	 {
	 var d=new Date(cadenaFecha.substring(6,10), cadenaFecha.substring(3,5)-1, cadenaFecha.substring(0,2), 0, 0, 0, 0)	
	 return d;
}

</script>
