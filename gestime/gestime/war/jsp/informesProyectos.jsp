<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Set"%>
<%@ page import="java.util.Collections"%>
<%@ page import="com.tecnificados.gestime.bean.EventoGC"%>
<%@ page import="com.tecnificados.gestime.bean.Proyecto"%>
<%@ page import="com.tecnificados.gestime.bean.Imputacion"%>
<%@ page import="java.text.SimpleDateFormat"%>



<%
	SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

ArrayList<Proyecto> proyectos=new ArrayList<Proyecto>();
Map <String, ArrayList<Imputacion>> mapaProyectoImputacion= new HashMap<String, ArrayList<Imputacion>>();

Map <String, String> mapaProyectoHoras = new HashMap<String, String>();

Map <String, ArrayList<String>> mapaProyectoFechaImputacion= new HashMap<String, ArrayList<String>>();

String pId="";
String pNombre="";

String errorBD="";

if (request.getParameter("errorBD")!=null){
	errorBD=request.getParameter("errorBD");
	
}


if (errorBD.equals(""))
{
	if (request.getSession(true).getAttribute("proyectos")!=null)
		proyectos=(ArrayList<Proyecto>)request.getSession(true).getAttribute("proyectos");
	
	
	if (request.getSession(true).getAttribute("mapaProyectoImputacion")!=null)
		mapaProyectoImputacion=(Map <String, ArrayList<Imputacion>> )request.getSession(true).getAttribute("mapaProyectoImputacion");
	
	if (request.getSession(true).getAttribute("mapa")!=null)
		mapaProyectoHoras=(Map <String, String> )request.getSession(true).getAttribute("mapa");
	
	if (request.getSession(true).getAttribute("mapaProyectoFechaImputacion")!=null)
		mapaProyectoFechaImputacion=(Map <String, ArrayList<String>> )request.getSession(true).getAttribute("mapaProyectoFechaImputacion");

}
%>



<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">



</head>

<body>



	<jsp:include page="menu.jsp"></jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h2>Informes de tus Proyectos</h2>
		</div>
	</div>

	<div class="container">
	
	
		<div class="row">&nbsp;</div>
		
		<%if (errorBD.equals("true")){ %>
		
			<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Se ha producido un error de base de datos. Por favor contacte con el Administrador.</div>
		
		<%} else { %>
		
		
		<div class="row">
			<select class="form-control" id="proyectoFiltro" onchange="gestionaCapas()">
				<option value="">Selecciona un proyecto para ver sólo su información.</option>
				<%
					for (Proyecto p:proyectos){
				%>
				<option value="<%=p.getId()%>"><%=p.getNombre()%></option>
				<%
					}
				%>
			</select>
		</div>


		<%
			Set<String> nombreProyectos=mapaProyectoHoras.keySet();
				ArrayList<String> listaNombres=new ArrayList<String>();
				for (String n:nombreProyectos)
				{
			listaNombres.add(n.substring(n.lastIndexOf("(")));
				}	
				Collections.sort(listaNombres);
			
				int contador=0;
				for (String n:listaNombres){
		%>
		<div id="info_P<%=proyectos.get(contador).getId()%>">
			<div class="row">
				<h3>
					<span class="label label-primary"><%=proyectos.get(contador).getNombre()%>&nbsp;(<%=proyectos.get(contador).getId()%>)</span>
				</h3>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<h4>
							<span class="label label-primary">Estado imputaciones</span>
						</h4>
					</div>
					<div id="piechart<%=n%>" style="height: 400px"></div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<h4>
							<span class="label label-primary">Imputaciones por usuario</span>
						</h4>
					</div>
					<div id="piechart2<%=n%>" style="height: 400px"></div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-6">
					<div class="row">
						<h4>
							<span class="label label-primary">Fecha de imputaciones</span>
						</h4>
					</div>
					<div id="linechart<%=n%>" style="height: 400px"></div>
				</div>
				<div class="col-md-6">
					<div class="row">
						<h4>
							<span class="label label-primary">Datos</span>
						</h4>
					</div>
					<div class="row right">
						<h5>
							<span class="label label-primary">Estado imputaciones</span>
						</h5>
					</div>
					<div class="row">
						<div id='tablaDatos<%=n%>1'></div>
					</div>
					<div class="row right">
						<h5>
							<span class="label label-primary">Imputaciones por usuario</span>
						</h5>
					</div>
					<div class="row">
						<div id='tablaDatos<%=n%>2'></div>
					</div>
					<div class="row right">
						<h5>
							<span class="label label-primary">Fecha de imputaciones</span>
						</h5>
					</div>
					<div class="row">
						<div id='tablaDatos<%=n%>3'></div>
					</div>
				</div>
			</div>
		</div>

		<%
			contador++;
				}
		%>

		<%
			nombreProyectos=mapaProyectoHoras.keySet();
				listaNombres=new ArrayList<String>(nombreProyectos);	
				Collections.sort(listaNombres);
				contador=0;
				for (String n:listaNombres)
				{			
			String horas=mapaProyectoHoras.get(n);
			if (horas!=null)
			{
			String horasImputadas=horas.substring(0,horas.indexOf(" "));
			String horasTotales=horas.substring(horas.indexOf(" "));
		%>

		<div class="row hide">
			<table id="Proyecto_<%=n%>">
				<thead>
					<th>Proyecto</th>
					<th>horas</th>
				</thead>
				<tr>
					<td>Imputadas</td>
					<td><%=horasImputadas%></td>
				</tr>
				<tr>
					<td>Restantes</td>
					<td><%=horasTotales%></td>
				</tr>
			</table>
		</div>

		<%
			}
		%>
		<%
			}
		%>



		<%
			nombreProyectos=mapaProyectoImputacion.keySet();
				listaNombres=new ArrayList<String>(nombreProyectos);	
				Collections.sort(listaNombres);
				contador=0;
				for (String n:listaNombres)
				{			
			
			
			ArrayList<Imputacion> imputaciones=mapaProyectoImputacion.get(n);
			if (imputaciones.size()>0){
		%>

		<div class="row hide">
			<table id="Imputaciones_<%=n%>">
				<thead>
					<th>Fecha</th>
					<th>Responsable</th>
					<th>horas</th>
				</thead>
				<%
					for (Imputacion i:imputaciones)
					{
				%>
				<tr>
					<td><%=i.getFecha()%></td>
					<td><%=i.getUsuario()%></td>
					<td><%=i.getHoras()%></td>
				</tr>


				<%
					}
				%>
			</table>
		</div>

		<%
			}
		%>
		<%
			}
		%>




		<%
			nombreProyectos=mapaProyectoFechaImputacion.keySet();
				listaNombres=new ArrayList<String>(nombreProyectos);	
				Collections.sort(listaNombres);
				contador=0;
				for (String n:listaNombres)
				{			
			ArrayList<String> imputaciones=mapaProyectoFechaImputacion.get(n);
			if (imputaciones.size()>0){
		%>

		<div class="row hide">
			<table id="FechasI_<%=n%>">
				<thead>
					<th>Fecha</th>
					<th>Imputaciones</th>
				</thead>
				<%
					for (String cadena:imputaciones)
					{
						if (!cadena.equals(" 0")){
				%>
				<tr>
					<td><%=cadena.substring(0,cadena.indexOf(" "))%></td>
					<td><%=cadena.substring(cadena.indexOf(" ")+1)%></td>
				</tr>

				<%
					}else{
				%>
				<tr>
					<td></td>
					<td></td>
				</tr>
				<%
					}
				%>
				<%
					}
				%>
			</table>
		</div>

		<%
			}
		%>
		<%
			}
		%>









<%
			}
		%>


		<hr>
	</div>
	<jsp:include page="pie.jsp"></jsp:include>




	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>

	<script type="text/javascript" src="https://www.google.com/jsapi"></script>
	<script type="text/javascript">
		google.load("visualization", "1", {
			packages : [ "corechart" ]
		});
		google.load('visualization', '1', {
			packages : [ 'table' ]
		});
	</script>


</body>
</html>
<script>
	var arrayProyectos;

	$(function() {

		arrayProyectos = new Array();
		arrayIdProyectos = new Array();
		arrayImputaciones = new Array();
		arrayIdImputaciones = new Array();
		arrayFechas = new Array();

		$("[id^=Proyecto_]").each(
				function() {
					var arrayTabla = new Array();
					var idTabla = $(this).attr('id');
					var idProyecto = idTabla
							.substring(idTabla.lastIndexOf("("));

					var horasRestantes = Number(getCellValues(idTabla, 2, 1))
							- Number(getCellValues(idTabla, 1, 1));

					arrayTabla.push(new Array("Tipo", "Horas"))
					arrayTabla.push(new Array(getCellValues(idTabla, 1, 0),
							Number(getCellValues(idTabla, 1, 1))))

					if (horasRestantes >= 0)
						arrayTabla.push(new Array(getCellValues(idTabla, 2, 0),
								horasRestantes))

					arrayProyectos.push(arrayTabla);

					arrayIdProyectos.push(idTabla);

					drawChart(arrayTabla, idProyecto);
				});

		var contador = 0;
		var usuario = "";
		var horas = 0;
		$("[id^=Imputaciones_]").each(function() {
			var arrayTabla = new Array();
			var arrayTablaFechas = new Array();
			var idTabla = $(this).attr('id');
			var idProyecto = idTabla.substring(idTabla.lastIndexOf("_") + 1);

			arrayTabla.push(new Array("Responsable", "Horas"))

			filasTabla = document.getElementById(idTabla).rows.length;

			var suma = 0;
			var usuario = getCellValues(idTabla, 1, 1);
			for (var i = 1; i < filasTabla; i++) {
				if (usuario == new Array(getCellValues(idTabla, i, 1))) {
					suma += Number(getCellValues(idTabla, i, 2));
				} else {
					arrayTabla.push(new Array(usuario, suma));
					usuario = getCellValues(idTabla, i, 1);
					suma = Number(getCellValues(idTabla, i, 2));
				}
			}
			arrayTabla.push(new Array(usuario, suma));

			arrayImputaciones.push(arrayTabla);

			arrayIdImputaciones.push(idTabla);

			drawChart2(arrayTabla, idProyecto);

		});

		$("[id^=FechasI_]").each(
				function() {
					var arrayTabla = new Array();
					var idTabla = $(this).attr('id');
					var idProyecto = idTabla
							.substring(idTabla.lastIndexOf("_") + 1);

					arrayTabla.push(new Array("Fecha", "Imputaciones"))

					filasTabla = document.getElementById(idTabla).rows.length;

					if (filasTabla > 1) {
						for (var i = 1; i < filasTabla; i++) {
							arrayTabla.push(new Array(getCellValues(idTabla, i,
									0), Number(getCellValues(idTabla, i, 1))));
						}
						arrayFechas.push(arrayTabla);

					}

					drawChart3(arrayTabla, idProyecto);

				});

	});

	function getCellValues(id, fila, columna) {
		var table = document.getElementById(id);
		return (table.rows[fila].cells[columna].innerHTML);
	}

	function drawChart(arrayDatos, idProyecto) {

		var data = google.visualization.arrayToDataTable(arrayDatos);
		var options = {
			is3D : true,
			colors : [ '#428BCA', '#F0AD4E' ]
		};

		var chart = new google.visualization.PieChart(document
				.getElementById("piechart" + idProyecto));
		chart.draw(data, options);

		drawTable(data, 'tablaDatos' + idProyecto + '1')
	}

	function drawChart2(arrayDatos, idProyecto) {

		var data = google.visualization.arrayToDataTable(arrayDatos);
		var options = {
			pieHole : 0.2
		};

		var chart = new google.visualization.PieChart(document
				.getElementById('piechart2(' + idProyecto + ')'));
		chart.draw(data, options);

		drawTable(data, 'tablaDatos(' + idProyecto + ')2')
	}

	function drawChart3(arrayDatos, idProyecto) {

		var data = google.visualization.arrayToDataTable(arrayDatos);

		var options = {
			title : 'Imputaciones por fecha'
		};

		var chart = new google.visualization.LineChart(document
				.getElementById('linechart(' + idProyecto + ')'));
		chart.draw(data, options);

		drawTable(data, 'tablaDatos(' + idProyecto + ')3')
	}

	function drawTable(data, id) {
		var table = new google.visualization.Table(document.getElementById(id));
		table.draw(data, {
			showRowNumber : true
		});
	}

	function gestionaCapas() {
		valor = ($('#proyectoFiltro').val())
		console.log(valor)

		if (valor == "") {
			$("[id^=info_P]").each(function() {
				$(this).removeClass("hide");
			});
		} else {
			$("[id^=info_P]").each(function() {
				$(this).addClass("hide");
			});

			//alert($("#info_P"+valor).attr('class'))

			$("#info_P" + valor).removeClass("hide");

		}
	}
</script>
