<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.tecnificados.gestime.bean.EventoGC"%>
<%@ page import="com.tecnificados.gestime.bean.Proyecto"%>
<%@ page import="com.tecnificados.gestime.bean.Imputacion"%>

<%
String idCalendario="";

if (request.getSession(true).getAttribute("idCalendario")!=null)
	idCalendario=(String)request.getSession(true).getAttribute("idCalendario");


%>





<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">



<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">


</head>

<body>



 	<jsp:include page="menu.jsp" ></jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h2>Visualización en Google Calendar</h2>			
		</div>
	</div>

	<div class="container">
	
		<div class="row separador"></div>
			<h3><span class="label label-primary">Tu calendario</span></h3>
		<div class="row-fluid">
			<div class="col-md-12" id="capaCalendario">
			<%if(!idCalendario.equals("")) {%>
				<iframe src="https://www.google.com/calendar/embed?src=<%=idCalendario%>&ctz=Europe/Madrid" style="border: 0;overflow:hidden;height:600px;width:100%" height="600px" width="100%" frameborder="0" scrolling="no"></iframe>
            <%} %>                 
            </div>       
		</div>	
			
			
		
</div>
  <jsp:include page="pie.jsp" ></jsp:include>




		<!-- Bootstrap core JavaScript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>		 
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="/js/bootstrap.min.js"></script>
</body>
</html>
<script>


	$(function() {
		
		
	});
	
	
	

	
	

</script>