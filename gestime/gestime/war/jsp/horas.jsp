<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.tecnificados.gestime.bean.EventoGC"%>
<%@ page import="com.tecnificados.gestime.bean.Proyecto"%>
<%@ page import="com.tecnificados.gestime.bean.Imputacion"%>

<%

SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

java.util.Date fechaActual = new Date();

String idCalendario="";

String errorBD="";
String errorGC="";
String errorParseo="";

boolean error=false;

String rSeleccion="";


if (request.getParameter("errorBD")!=null){
	errorBD=request.getParameter("errorBD");
	error=true;
}

if (request.getParameter("errorGC")!=null){
	errorGC=request.getParameter("errorGC");
	error=true;
}

if (request.getParameter("errorParseo")!=null){
	errorParseo=request.getParameter("errorParseo");
	error=true;
}


ArrayList<Proyecto> proyectos=new ArrayList<Proyecto>();
ArrayList<Imputacion> imputaciones=new ArrayList<Imputacion>();


if (!error)
{
	if (request.getSession(true).getAttribute("proyectos")!=null)
		proyectos=(ArrayList<Proyecto>)request.getSession(true).getAttribute("proyectos");	
	
	if (request.getSession(true).getAttribute("imputaciones")!=null)
		imputaciones=(ArrayList<Imputacion>)request.getSession(true).getAttribute("imputaciones");
	
	if (request.getSession(true).getAttribute("idCalendario")!=null)
		idCalendario=(String)request.getSession(true).getAttribute("idCalendario");
	
	if (request.getParameter("rSeleccion")!=null){
		rSeleccion=request.getParameter("rSeleccion");	
	}

}




%>





<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">



<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">


</head>

<body>



	<jsp:include page="menu.jsp"></jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h2>Registro de horas</h2>
		</div>
	</div>

	<div class="container">

		<div class="row separador">
			<h3>
				<span class="label label-primary">Nueva Imputación</span>
			</h3>
		</div>

		<div class="row">
			<form class="form-horizontal" role="form" id="formProyecto" name="formProyecto" method="post" action="gesinput">
				<input type="hidden" id="accion" name="accion" value="alta"> 
				<input type="hidden" id="idCalendario" name="idCalendario" value="<%=idCalendario%>"> 
				<input type="hidden" name="proyectoId" id="proyectoId" value="">
				<input type="hidden" name="seleccionFechas" id="seleccionFechas" value="">
				<div class="form-group">
					<label for="proyecto" class="col-md-1 control-label">Proyecto</label>
					<div class="col-md-10">
						<select class="form-control" name="proyecto" id="proyecto" required autofocus>
							<option value="">Selecciona un proyecto</option>
							<%for (Proyecto p:proyectos){ 
								if (p.getFechaFin().after(fechaActual))
								{%>
							<option value="<%=p.getId()%>"><%=p.getNombre()%></option>
							<%}%>
							<%}%>
						</select>
					</div>
					<div class="col-md-1"></div>
				</div>

				<div class="form-group">
					<label for="proyecto" class="col-md-1 control-label">Día</label>
					<div class="col-md-2">
						<input class="form-control" type="text" name="dia" id="dia" maxlength="10" required>
					</div>
					<label for="proyecto" class="col-md-1 control-label">De</label>
					<div class="col-md-1">
						<input class="form-control" type="text" name="horaI" id="horaI" maxlength="5" value="09:00">
					</div>
					<div class="col-md-1">
						<label for="proyecto" class="control-label">a</label>
					</div>
					<div class="col-md-1">
						<input class="form-control" type="text" name="horaF" id="horaF" maxlength="5" value="18:00">
					</div>
					<div class="col-md-1">
						<label for="horas" class="control-label">Horas</label>
					</div>
					<div class="col-md-1">
						<input class="form-control" type="number" step="0.5" name="horas" id="horas" maxlength="2" value="8">
					</div>
				</div>
				<div class="form-group">
					<label for="proyecto" class="col-md-1 control-label">Descripción</label>
					<div class="col-md-10">
						<textarea class="form-control" name="descripcion" id="descripcion" rows="4" maxlength="500" required></textarea>
					</div>
					<div class="col-md-1"></div>
				</div>
				<div class="form-group">
					<div class="col-md-3"></div>
					<div class="col-md-3">
						<button class="btn btn-primary btn-block" onclick="alta();return false;" id="botonAlta">Alta</button>
						<button class="hide" type="submit" id="botonSubmit">enviar</button>
					</div>
				</div>
			</form>
		</div>


		<div class="alert alert-danger hide" id="avisoSincro" style="text-align: center">Error: las imputaciones no están sincronizadas. Pongase en contacto con el Administrador.</div>

		<%if (errorBD.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Se ha producido un error de base de datos. Por favor contacte con el Administrador.</div>
		<%} %>

		<%if (errorGC.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Se ha producido un error en Google Calendar. Por favor inténtelo de nuevo más tarde.</div>
		<%} %>

		<%if (errorParseo.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Los datos no están llegado correctamente. Por favor contacte con el Administrador.</div>
		<%} %>




		<div class="row separador">

			<h3>
				<span class="label label-primary">Imputaciones Anteriores</span>
			</h3>
		</div>



		<form class="form-horizontal" role="form" method="post" id="formImputaciones" name="formImputaciones" method="post" action="gesinput">
			<input type="hidden" id="imputaciones" name="imputaciones" value=""> 
			<input type="hidden" id="accion" name="accion" value="baja"> <input type="hidden" id="idCalendario"	name="idCalendario" value="<%=idCalendario%>">
			<div class="form-group">
				<div class="radio col-md-1"></div>
				<div class="radio col-md-2">
					<label> <input type="radio" name="rSeleccion" value="all" onchange="refrescaImputaciones()"> Todas
					</label>
				</div>
				<div class="radio col-md-2">
					<label> <input type="radio" name="rSeleccion" value="mes" onchange="refrescaImputaciones()"> Mes
					</label>
				</div>
				<div class="radio col-md-2">
					<label> <input type="radio" name="rSeleccion" value="semana" checked="checked" onchange="refrescaImputaciones()"> Semana
					</label>
				</div>
				<div class="radio col-md-2">
					<label> <input type="radio" name="rSeleccion" value="hoy" onchange="refrescaImputaciones()"> Hoy
					</label>
				</div>
				<div class="col-md-2">
					<button class="btn btn-primary" onclick="eliminar();return false;" id="botonEliminar">Eliminar</button>
				</div>
			</div>
		</form>

		<div id="listadoImputacionesBBDD" class="form-group">


			<%
					int contador=0;
					for (Imputacion i:imputaciones){ 
						
						
						String descripcion=i.getDescripcion();
						if (descripcion.length()>=80)
							descripcion=descripcion.substring(0,80)+"...";						
						
						descripcion=formatoFecha.format(i.getFecha())+" - "+ i.getProyecto() + " - " + i.getHoras() + " horas "  +" - "+ descripcion;
						
						
						
						String linea="<div class='checkbox' id='capaCK"+contador+"'>";
						
						String estado="";
						if (i.getEstado()==1)
							estado+="<span class='label label-success'>Aprobado</span>";
						else if (i.getEstado()==-1)
							estado+="<span class='label label-danger'>Rechazada</span>";
						else
							estado+="<span class='label label-info'>Pendiente</span>";
						
						linea+="<span title='"+i.getDescripcion()+"'></span><input type='radio' name='imputacionesBBDD' onchange='javascript:cambiaCK(this)' id='ckBD"+contador+"' value='"+i.getId()+"'>&nbsp;"+descripcion+"</div>";
						
						linea="<div class='row '><div class='col-md-1'>&nbsp;</div><div id='statusINFO"+(contador++)+"' class='col-md-1 margin1010'>"+estado+"</div><div class='col-md-10'>"+linea+"</div></div>";
						
						
						
					%>
			<%=linea%>



			<%} %>

		</div>




		<!-- Imputaciones de google calendar -->
		<div id="listadoImputaciones" class="form-group hide"></div>






	</div>
	<jsp:include page="pie.jsp"></jsp:include>



	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
	<script src="/js/bootstrap.min.js"></script>
</body>
</html>
<script>
var imputaciones;

	$(function() {
		
		$.datepicker.regional['es'] = {
		        closeText: 'Cerrar',
		        prevText: '<Ant',
		        nextText: 'Sig>',
		        currentText: 'Hoy',
		        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
		        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
		        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
		        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
		        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
		        weekHeader: 'Sm',
		        dateFormat: 'dd/mm/yy',
		        firstDay: 1,
		        isRTL: false,
		        showMonthAfterYear: false,
		        yearSuffix: ''
		    };
		$.datepicker.setDefaults($.datepicker.regional['es']);
		
		$('#horaI,#horaF').change(function() {

			var dateHoraI = cadenaToDate($("#dia").val()+" "+$("#horaI").val());

			var dateHoraF = cadenaToDate($("#dia").val()+" "+$("#horaF").val());	




			//Para calcular la diferencia se trabaja con milisegundos

			var difference_ms = Number(dateHoraF.getTime()) - Number(dateHoraI.getTime());

			

			$('#horas').val(difference_ms/(1000*60*60));

		});    
		    
		    
		
		imputaciones = eval(<%=(String)request.getSession().getAttribute("eventos")%>);
		
		
		


		$("input[name='rSeleccion'][value='<%=rSeleccion%>']").attr("checked", true);


		
		refrescaImputaciones();		
		
		$("#dia").datepicker({dateFormat: 'dd/mm/yy'});
		$("#dia").val(formattedDate());
		
		
		<%if (!error) {%>
		
		if ((imputaciones.eventos.length!=<%=imputaciones.size()%>))
		{
			//alert("Las imputaciones de la base de datos y de Google Calendar no están sincronizadas")	
			
			$("#avisoSincro").removeClass("hide");
			
			$("#botonEliminar").prop( "disabled", true );
			$("#botonAlta").prop( "disabled", true );
		}
		
		<%}%>
		
	});
	
	
	

	
	function refrescaImputaciones()
	{
		
		var valor=$("input[name='rSeleccion']:checked").val();
		var fecha_actual = new Date();
		var mes=new String(fecha_actual.getMonth()+1);
		var anio=new String(fecha_actual.getFullYear());
		var hoy=new String(fecha_actual.getDate())
	
		if (hoy.length==1)
			hoy="0"+hoy;
		
		if (mes.length==1)
			mes="0"+mes;
		
		//alert(" es " + dias_semana[fecha_actual.getDay()] + " dia " + fecha_actual.getDate() + " de " + meses[fecha_actual.getMonth()] + " de " + fecha_actual.getFullYear());
		
		
		//Oculto las que vienen de BBDD
		$("[id^=capaCK]").each(function () {
		    $(this).addClass("hide");		    
		  });

		$("[id^=statusINFO]").each(function () {
		    $(this).addClass("hide");		    
		  });
		
		
		
		
		if (valor=="all")
		{
			$("#listadoImputaciones").empty();
			//Muestro todas las de Google
			for(f=0;f<imputaciones.eventos.length;f++)
			{				
				$("#listadoImputaciones").append(addCheckBox('ck_'+f, imputaciones.eventos[f].Evento.id, imputaciones.eventos[f].Evento.inicio+" "+imputaciones.eventos[f].Evento.fin+" "+imputaciones.eventos[f].Evento.proyecto+" "+imputaciones.eventos[f].Evento.descripcion));
			}
			//Muestro todas las de BBDD
			$("[id^=capaCK]").each(function () {
			    $(this).removeClass("hide");		    
			});
			
			$("[id^=statusINFO]").each(function () {
			    $(this).removeClass("hide");		    
			  });
		}
		else if (valor=="mes")
		{	
			$("#listadoImputaciones").empty();
			for(f=0;f<imputaciones.eventos.length;f++)
			{
				var inicio=new String(imputaciones.eventos[f].Evento.inicio);
				inicio=inicio.substring(3,inicio.length);
				inicio=inicio.substring(0,7);
				
				if (inicio==mes+"/"+anio){					
					$("#listadoImputaciones").append(addCheckBox('ck_'+f, imputaciones.eventos[f].Evento.id, imputaciones.eventos[f].Evento.inicio+" "+imputaciones.eventos[f].Evento.fin+" "+imputaciones.eventos[f].Evento.proyecto+" "+imputaciones.eventos[f].Evento.descripcion));
					$("#capaCK"+f).removeClass("hide");
					$("#statusINFO"+f).removeClass("hide");
				}
			}	
		}		
		else if (valor=="semana")
		{
			//Calculo los días de la semana
			var semana=new Array();			
			for (var i=1;i<=7;i++)
			{
				var day;				
				if (fecha_actual.getDay()==0)
					day = new String(fecha_actual.getDate() - 7 + i);
				else
					day = new String(fecha_actual.getDate() - fecha_actual.getDay() + i);
				
				
				if (day.length==1)
					day="0"+day;	
				semana.push(day+"/"+mes+"/"+anio)
			}
			
			
			$("#listadoImputaciones").empty();
			for(f=0;f<imputaciones.eventos.length;f++)
			{
				
				var inicio=new String(imputaciones.eventos[f].Evento.inicio);
				inicio=inicio.substring(0,10);
			
				if (semana.indexOf(inicio)>=0){					
					$("#listadoImputaciones").append(addCheckBox('ck_'+f, imputaciones.eventos[f].Evento.id, imputaciones.eventos[f].Evento.inicio+" "+imputaciones.eventos[f].Evento.fin+" "+imputaciones.eventos[f].Evento.proyecto+" "+imputaciones.eventos[f].Evento.descripcion));
					$("#capaCK"+f).removeClass("hide");
					$("#statusINFO"+f).removeClass("hide");
				}
			}
		}	
		else if (valor=="hoy")
		{
			
			
			$("#listadoImputaciones").empty();
			for(f=0;f<imputaciones.eventos.length;f++)
			{
				var today=hoy+"/"+mes+"/"+anio;
				var inicio=new String(imputaciones.eventos[f].Evento.inicio);
				inicio=inicio.substring(0,10);		
		
				if (today==inicio){					
					$("#listadoImputaciones").append(addCheckBox('ck_'+f, imputaciones.eventos[f].Evento.id, imputaciones.eventos[f].Evento.inicio+" "+imputaciones.eventos[f].Evento.fin+" "+imputaciones.eventos[f].Evento.proyecto+" "+imputaciones.eventos[f].Evento.descripcion));
					$("#capaCK"+f).removeClass("hide");	
					$("#statusINFO"+f).removeClass("hide");
				}
			}
		}
		
	}
	
	function eliminar()
	{
		
		
		
		var arrayIds=new Array();
		$("input:checkbox").each(function(){			
		    var control = $(this);
		    if(control.is(":checked")){
		    	arrayIds.push(control.attr("value"));
		    }
		});
		
		$("input:radio").each(function(){			
		    var control = $(this);		    
		    if((control.is(":checked"))&&(control.attr('id')!=null)){
		    	if ($(this).attr('id').indexOf("ckBD")==0)
		    		{
		    		arrayIds.push(control.attr("value"));
		    		}
		    		
		    }
		});
		

		
		
		
		
		
		if (arrayIds.length>0)
		{
			
			imputaciones="";
			for(var i=0;i<arrayIds.length;i++)
				imputaciones+=arrayIds[i]+"||";
			
			
			
			
			$("#imputaciones").val(imputaciones);			
			
			$("#formImputaciones").submit();
			/*
			parametros=parametros.substring(0,parametros.length-2);
			
			console.log("/gesinput?action=baja"+parametros);
			
			var jqxhr = $.ajax( "/gesinput?action=baja"+parametros )			
			.done(function(data) {
				
				if (data.estado=="ok")
					refrescarProyectos();
			})
			.fail(function() {
			alert( "error" );
			})
			*/
		}
		else
			alert("No hay seleccionado ninguna imputacion para eliminar");
		
	}
	
	function alta()
	{	
		var ok=true;
		
		
		
		ok=($('#proyecto')[0].checkValidity());
		
		if (ok)
			ok=($('#descripcion')[0].checkValidity());
		
		if ((ok)&&($("#proyecto").val()==-1))
		{
				alert("Selecciona un proyecto para continuar");
				ok=false;
		}	
		
		
		if ((ok)&&(!formatTime($("#horaI").val())))
		{
			alert("La hora inicial no es correcta, sigue el formato HH:MM como 12:35");
			ok=false;
		}
		
		if ((ok)&&(!formatTime($("#horaF").val())))
		{
			alert("La hora final no es correcta, sigue el formato HH:MM como 12:35");
			ok=false;
		}
		
		if ((ok)&&($("#descripcion").val()==""))
		{
			alert("Escribe una descripción para continuar");
			ok=false;
		}
		
		if (ok)
		{
		
			//Compruebo si las horas se solapan
			
			//alert(imputaciones.eventos.length)
			
			fechaI=$("#dia").val()+" "+$("#horaI").val();
			fechaF=$("#dia").val()+" "+$("#horaF").val();		
			
					
			
			var d1 = cadenaToDate(fechaI);
			
			var d2 = cadenaToDate(fechaF);		
			
			if (d1>=d2)
			{	
				alert("la primera fecha no puede ser posterior a la segunda")
				ok=false;
			}
			
			if (ok)
			{
				for(f=0;f<imputaciones.eventos.length;f++)
				{
								
					var n1=cadenaToDate(imputaciones.eventos[f].Evento.inicio)
					var n2=cadenaToDate(imputaciones.eventos[f].Evento.fin)
					
					
					if (colisionan(d1,d2,n1,n2))
						{
						alert("Quieres imputar un periodo que ya se ha utilizado")						
						return;
						}
					
				}
			}
		}
		
		
		
		if (!ok)			
			$("#botonSubmit").click();
		else{						
			$("#proyectoId").val($("#proyecto option:selected").text());
			$("#seleccionFechas").val($('input:radio[name=rSeleccion]:checked').val());
			
			
			
			$("#formProyecto" ).submit();
		}
	
		
		return;
		
		var parametros="&descripcion="+$("#descripcion").val();
		parametros+="&proyectoId="+$("#proyecto").val();
		parametros+="&proyecto="+$("#proyecto option:selected").text();
		parametros+="&dia="+$("#dia").val();
		parametros+="&horaI="+$("#horaI").val();
		parametros+="&horaF="+$("#horaF").val();
		parametros+="&horas="+$("#horas").val();
		parametros+="&usuario=<%=request.getSession().getAttribute("userName")%>";

		var jqxhr = $.ajax("/gesinput?accion=alta" + parametros).done(
				function(data) {

					if (data.estado == "ok")
						refrescarProyectos();
				}).fail(function(jqXHR, textStatus) {
			alert("error: " + textStatus);
		})

	}

	function refrescarProyectos() {

		$("#listadoImputaciones").empty();

		var jqxhr = $.ajax("/gesinput?action=listadoImputaciones").done(
				function(data) {
					imputaciones = eval(data);
					refrescaImputaciones();
				}).fail(function() {
			alert("error");
		})

	}

	function formattedDate(date) {
		var d = new Date(date || Date.now()), month = '' + (d.getMonth() + 1), day = ''
				+ d.getDate(), year = d.getFullYear();

		if (month.length < 2)
			month = '0' + month;
		if (day.length < 2)
			day = '0' + day;

		return [ day, month, year ].join('/');
	}

	function formatTime(str) {

		var patron = /^(0[1-9]|1\d|2[0-3]):([0-5]\d)$/;

		return patron.test(str);
	}

	//Recibe una cadena con formato dd/mm/aaaa hh:mm
	function cadenaToDate(cadenaFecha) {

		var d = new Date(cadenaFecha.substring(6, 10), cadenaFecha.substring(3,
				5) - 1, cadenaFecha.substring(0, 2), cadenaFecha.substring(11,
				13), cadenaFecha.substring(14, 16), 0, 0)

		return d;
	}

	function colisionan(fechaIni1, fechaFin1, fechaIni2, fechaFin2) {
		c1 = fechaIni1 < fechaFin2;
		c2 = fechaIni1 < fechaIni2;
		c3 = fechaFin1 < fechaFin2;
		c4 = fechaFin1<fechaIni2;
		
		c5=fechaIni1>fechaFin2;
		c6 = fechaIni1 > fechaIni2;
		c7 = fechaFin1 > fechaFin2;
		c8 = fechaFin1 > fechaIni2;

		if (c1 && c2 && c3 && c4)
			return false;

		if (c5 && c6 && c7 && c8)
			return false;

		return true;

	}

	function addCheckBox(id, value, text) {

		var linea = "<div class='checkbox'><label><input type='checkbox' id='"+id+"' value='"+value+"'>"
				+ text + "</label></div>";
		linea = "<div class='row'><div class='col-md-offset-1 col-md-10'>"
				+ linea + "</div></div>"
		return linea;
	}

	function cambiaCK(control) {
		var id = control.id;
		var idGoogle = id.replace('ckBD', 'ck_')

		var marcado = $("#" + id).is(':checked');

		if (marcado)
			$("#" + idGoogle).prop('checked', true);
		else
			$("#" + idGoogle).prop('checked', false);
	}

	/*
	$('#formProyecto').submit(function(event){
	    // cancels the form submission
	    event.preventDefault();
	    // do whatever you want here
	});
	 */
</script>