<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="com.tecnificados.gestime.bean.EventoGC"%>
<%@ page import="com.tecnificados.gestime.bean.Proyecto"%>



<%


String errorBD="";
String errorParseo="";

boolean error=false;

String rSeleccion="";


if (request.getParameter("errorBD")!=null){
	errorBD=request.getParameter("errorBD");
	error=true;
}

if (request.getParameter("errorParseo")!=null){
	errorParseo=request.getParameter("errorParseo");
	error=true;
}


ArrayList<Proyecto> proyectos=new ArrayList<Proyecto>();

Map <String, String> mapaProyectoHoras = new HashMap<String, String>();

if (!error)
{
if (request.getSession(true).getAttribute("proyectos")!=null)
	proyectos=(ArrayList<Proyecto>)request.getSession(true).getAttribute("proyectos");

if (request.getSession(true).getAttribute("mapa")!=null)
	mapaProyectoHoras=(Map <String, String> )request.getSession(true).getAttribute("mapa");
}
%>



<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">



</head>

<body>



	<jsp:include page="menu.jsp" ></jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h2>Gestión de Proyectos</h2>			
		</div>
	</div>

	<div class="container">
		<div class="row separador">
				<div class="col-md-6">
					<h3><span class="label label-primary">Nuevo Proyecto</span></h3>
				</div>
				<div class="col-md-6">					
					<h3><span class="label label-primary">Tus Proyectos</span></h3>
				</div>
		</div>
	
		<div class="row">	
			<div class="col-md-6" id="nuevoProyecto">			
				<div class="row">				
					<form class="form-horizontal" role="form" id="formProyecto" name="formProyecto" method="post" action="/gespro?accion=alta">
						<div class="form-group">
					    	<label for="nombre" class="col-md-3 control-label">Proyecto</label>
					    	<div class="col-md-9">
					        	<input class="form-control" type="text" name="nombre" id="nombre" maxlength="100" placeHolder="Nombre del proyecto, ej: 'Reforma Racoon City'" required>
					    	</div>				    	
					  	</div>
					  	<div class="form-group">
					    	<label for="proyecto" class="col-md-3 control-label">Jefe del Proyecto</label>
					    	<div class="col-md-9">
					        	<input class="form-control" type="text" name="jefe" id="jefe" maxlength="100" value="<%=request.getSession().getAttribute("userName")%>" disabled="true" required>
					    	</div>				    	
					  	</div>				  	
					  	<div class="form-group">
					    	<label for="fechaInicio" class="col-md-3 control-label">Fecha Inicio</label>
					    	<div class="col-md-3">
					        	<input class="form-control" type="text" name="fechaInicio" id="fechaInicio" maxlength="10" placeHolder="dd/mm/aaaa" required>
					    	</div>
					    	<div class="col-md-1">&nbsp;</div>
					    	<div class="col-md-2">
					    		<label for="fechaFin" class="control-label">Fecha Fin</label>
					    	</div>				    	
					    	<div class="col-md-3">
					        	<input class="form-control" type="text" name="fechaFin" id="fechaFin" maxlength="10" placeHolder="dd/mm/aaaa" required>
					    	</div>					    	
					  	</div>
					  	<div class="form-group">
					    	<label for="horas" class="col-md-3 control-label">Horas estimadas</label>
					    	<div class="col-md-3">
					        	<input  class="form-control" type="number" step="40" name="horas" id="horas" maxlength="4" value="1000" required>
					    	</div>					    	
					    	<div class="col-md-1">&nbsp;</div>
					    	<!--
					    	<div class="col-md-2">
					    		<label for="codigo" class="control-label">Código</label>
					    	</div>				    	
					    	<div class="col-md-3">
					        	<input class="form-control" type="text" name="codigo" id="codigo" maxlength="10" placeHolder="Código">
					    	</div>
					    	-->
					  	</div>
					  	<div class="form-group">
					  		<div class="col-md-8"></div>
					  		<div class="col-md-4">
					  			<button class="btn btn-primary btn-lg btn-block" id="botonAlta" onclick="alta()">Alta</button>
					  		</div>				  		
					  	</div>				  	
					</form>				
				</div>
			</div>	
		
			
				
			
					
			<div class="col-md-6" id="tusProyectos">
			<form name="formProyecto2" id="formProyecto2" method="post"  action="">				
				<div class="row">
					<div class="col-md-3"></div>
						<div class="col-md-9">
						  <ul class="list-group">				
							<%for (Proyecto p:proyectos){ %>
							
							<%if (mapaProyectoHoras.get(p.getId())!=null){%>
							
								<% if (!mapaProyectoHoras.get(p.getId()).equals("0.0")){%>
						 		<li class="list-group-item"><a title="editar" href="javascript:editar('<%=p.getId()%>')"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a title="borrar" href="javascript:noBorrar()"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;&nbsp;<%=p.getNombre()%> <span class="badge pull-right"><%=mapaProyectoHoras.get(p.getId())%> / <%=p.getHorasI()%></span></li>
						 		<% }else {%>
						 		<li class="list-group-item"><a title="editar" href="javascript:editar('<%=p.getId()%>')"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a title="borrar" href="javascript:borrar('<%=p.getId()%>')"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;&nbsp;<%=p.getNombre()%> <span class="badge pull-right"><%=mapaProyectoHoras.get(p.getId())%> / <%=p.getHorasI()%></span></li>
						 		<%} %>
							<%}else{ %>
								<li class="list-group-item"><a title="editar" href="javascript:editar('<%=p.getId()%>')"><span class="glyphicon glyphicon-pencil"></span></a>&nbsp;<a title="borrar" href="javascript:noBorrar()"><span class="glyphicon glyphicon-trash"></span></a>&nbsp;&nbsp;<%=p.getNombre()%> <span class="badge pull-right"><%=p.getHorasI()%></span></li>	  	
							<%}		  	
							}%>	
						</ul>
						</div>	
				</div>				
			</form>
				


						
				
				
			</div>
		</div>
	
		<div class="row separador">
			<div class="col-md-6">
				<h3><span class="label label-primary">Detalles</span></h3>
			</div>				
		</div>
		<%if (errorBD.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Se ha producido un error de base de datos. Por favor contacte con el Administrador.</div>
		<%} %>

		
		<%if (errorParseo.equals("true")){ %>
		<div class="alert alert-danger" id="avisoSincro" style="text-align: center">Error: Los datos no están llegado correctamente. Por favor contacte con el Administrador.</div>
		<%} %>
		
		<div class="row">	
			<div class="col-md-6" id="Detalles">
				<div class="row hide">
			  		<div class="col-md-1">&nbsp;</div>
			    	<div class="col-md-11 alert alert-info">Si dejas en blanco el código, se generará automáticamente.</div>
			  	</div>		  	  	
			  	<div class="row">
			  		<div class="col-md-1">&nbsp;</div>
			    	<div class="col-md-11 alert alert-info">Si la fecha de fin es superada no se podrá utilizar este proyecto.</div>
			  	</div>						  	
			</div>				  	
		</div>
		
		<hr>
	
		</div>
		  <jsp:include page="pie.jsp" ></jsp:include>
		<!-- /container -->



		<!-- Bootstrap core JavaScript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>		 
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
		<script src="/js/bootstrap.min.js"></script>
</body>
</html>
<script>
$(function() {
	
	
	$.datepicker.regional['es'] = {
	        closeText: 'Cerrar',
	        prevText: '<Ant',
	        nextText: 'Sig>',
	        currentText: 'Hoy',
	        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	        weekHeader: 'Sm',
	        dateFormat: 'dd/mm/yy',
	        firstDay: 1,
	        isRTL: false,
	        showMonthAfterYear: false,
	        yearSuffix: ''
	    };
	    $.datepicker.setDefaults($.datepicker.regional['es']);
	   
		    
		    
		    $("#fechaInicio, #fechaFin").datepicker({dateFormat: 'dd/mm/yy'});
			$("#fechaInicio, #fechaFin").val();
			
			
	<%if (error){%>
	
		$("#botonAlta").prop("disabled",true)
	
	<%}%>
	
});
/*
$('#formProyecto').submit(function(event){
    // cancels the form submission
    event.preventDefault();
    // do whatever you want here
});
*/
function alta()
{	
	var ok=true;	
	ok=($('#nombre')[0].checkValidity());
	
	if (ok)
		ok=($('#fechaInicio')[0].checkValidity());
	if (ok)
		ok=($('#fechaFin')[0].checkValidity());
	if (ok){
		$('#horas')[0].setCustomValidity("");
		ok=($('#horas')[0].checkValidity());
		if (!ok)
			$('#horas')[0].setCustomValidity("Introduzca un numero entero");
	}
	
	
	
	if (ok){		
		$("#jefe").prop('disabled', false);
		$("#formProyecto").submit();
	}
}

function borrar(id)
{	
	var r=confirm("Se va a borrar el proyecto seleccionado, ¿está seguro?");
	if (r==true)
	{
		 $("#formProyecto2").attr("action", "/gespro?accion=baja&codigo="+id);		 
		 $("#formProyecto2").submit();
	}	
}

function noBorrar()
{	
	alert("No se puede borrar un proyecto que tiene horas asignadas.");	
}

function editar(id)
{	
	 $("#formProyecto2").attr("action", "/gespro?accion=editar&codigo="+id);	 
	 $("#formProyecto2").submit();		
}

function refrescarProyectos()
{
	
}

</script>
