<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@page import="com.tecnificados.gestime.bbdd.ProyectoBD"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.Map"%>
<%@ page import="java.util.HashMap"%>
<%@ page import="java.util.Date"%>
<%@ page import="java.text.SimpleDateFormat"%>
<%@ page import="com.tecnificados.gestime.bean.EventoGC"%>
<%@ page import="com.tecnificados.gestime.bean.Proyecto"%>



<%

SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");

Proyecto proyecto=new Proyecto();

Date ultimaFecha=null;
String horasImputadas="0.0";


if (request.getSession(true).getAttribute("proyectos")!=null)
	proyecto=(Proyecto)request.getSession(true).getAttribute("proyecto");

if (request.getSession(true).getAttribute("ultimaFecha")!=null)
	ultimaFecha=(Date)request.getSession(true).getAttribute("ultimaFecha");

if (request.getSession(true).getAttribute("horasImputadas")!=null)
	horasImputadas=(String)request.getSession(true).getAttribute("horasImputadas");




%>



<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">
<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/themes/smoothness/jquery-ui.css" />
<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">


</head>

<body>



	<jsp:include page="menu.jsp" ></jsp:include>

	<!-- Main jumbotron for a primary marketing message or call to action -->
	<div class="jumbotron">
		<div class="container">
			<h2>Gestión de Proyectos</h2>			
		</div>
	</div>

	<div class="container">
		<div class="row separador">
				<div class="col-md-6">
					<h3><span class="label label-primary">Editar Proyecto</span></h3>
				</div>
				<div class="col-md-6">					
					<h3><span class="label label-primary">Detalles</span></h3>
				</div>
		</div>
	
		<div class="row">	
			<div class="col-md-6" id="nuevoProyecto">			
				<div class="row">				
					<form class="form-horizontal" role="form" id="formProyecto" name="formProyecto" method="post" action="/gespro?accion=modificar">
						<div class="form-group">
					    	<label for="nombre" class="col-md-3 control-label">Proyecto</label>
					    	<div class="col-md-9">
					        	<input class="form-control" type="text" name="nombre" id="nombre" maxlength="100" value="<%=proyecto.getNombre()%>" required>
					    	</div>				    	
					  	</div>
					  	<div class="form-group">
					    	<label for="proyecto" class="col-md-3 control-label">Jefe del Proyecto</label>
					    	<div class="col-md-9">
					        	<input class="form-control" type="text" name="jefe" id="jefe" maxlength="100" value="<%=proyecto.getJefe()%>" disabled="true" required>
					    	</div>				    	
					  	</div>				  	
					  	<div class="form-group">
					    	<label for="fechaInicio" class="col-md-3 control-label">Fecha Inicio</label>
					    	<div class="col-md-3">
					        	<input class="form-control" type="text" name="fechaInicio" id="fechaInicio" maxlength="10" value="<%=formatoFecha.format(proyecto.getFechaInicio())%>" disabled="true" required>
					    	</div>
					    	<div class="col-md-1">&nbsp;</div>
					    	<div class="col-md-2">
					    		<label for="fechaFin" class="control-label">Fecha Fin</label>
					    	</div>				    	
					    	<div class="col-md-3">
					        	<input class="form-control" type="text" name="fechaFin" id="fechaFin" maxlength="10" value="<%=formatoFecha.format(proyecto.getFechaFin())%>" required>
					    	</div>					    	
					  	</div>
					  	<div class="form-group">
					    	<label for="horas" class="col-md-3 control-label">Horas estimadas</label>
					    	<div class="col-md-3">
					        	<input  class="form-control" type="number" name="horas" id="horas" maxlength="4" min="<%=horasImputadas.substring(0,horasImputadas.indexOf("."))%>" value="<%=proyecto.getHorasI()%>" required>
					    	</div>					    	
					    	<div class="col-md-1">&nbsp;</div>
					    	
					    	<div class="col-md-2">
					    		<label for="codigo" class="control-label">Código</label>
					    	</div>				    	
					    	<div class="col-md-3">
					        	<input class="form-control" type="text" name="codigo" id="codigo" maxlength="10" value="<%=proyecto.getId()%>" disabled="true">
					    	</div>

					  	</div>
					  			  	  	
					  	<div class="form-group">
					  		<div class="col-md-3"></div>
					  		<div class="col-md-4">
					  			<button class="btn btn-primary btn-lg btn-block" onclick="javascript:volver()">Volver</button>
					  		</div>
					  		<div class="col-md-1">&nbsp;</div>
					  		<div class="col-md-4">
					  			<button class="btn btn-primary btn-lg btn-block" onclick="javascript:modificar()">Modificar</button>
					  		</div>				  		
					  	</div>				  	
					</form>				
				</div>
			</div>	
			<div class="col-md-6" id="Detalles Proyecto">							
				<div class="row">
				
					
					
					<%if (ultimaFecha!=null){ %>
						<div class="col-md-11 col-md-offset-1 alert alert-warning">La última imputación del proyecto es del día: <%=formatoFecha.format(ultimaFecha)%></div>
					<%}%> 
					
						
			    	<div class="col-md-11 col-md-offset-1 alert alert-warning">
			    	<%if (horasImputadas.equals("0.0")){ %>
						El proyecto no tiene ninguna hora imputada
					<%}else{%>
						El proyecto tiene un total de <%=horasImputadas%> horas imputadas.
					<%}%> 
			    	</div>
			  			
					<div class="col-md-11 col-md-offset-1 alert alert-info">Si la fecha de fin es superada no se podrá utilizar este proyecto.</div>
					
				</div>				
				
			</div>
		</div>
	
		
		
		
		<hr>
	
		</div>
		
		  <jsp:include page="pie.jsp" ></jsp:include>
		



		<!-- Bootstrap core JavaScript
    ================================================== -->
		<!-- Placed at the end of the document so the pages load faster -->
		<script	src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>		 
		<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.4/jquery-ui.min.js"></script>
		<script src="../jquery/development-bundle/ui/i18n/jquery.ui.datepicker-es.js"></script>
		<script src="/js/bootstrap.min.js"></script>
</body>
</html>
<script>
$(function() {
	
	
	$.datepicker.regional['es'] = {
	        closeText: 'Cerrar',
	        prevText: '<Ant',
	        nextText: 'Sig>',
	        currentText: 'Hoy',
	        monthNames: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
	        monthNamesShort: ['Ene','Feb','Mar','Abr', 'May','Jun','Jul','Ago','Sep', 'Oct','Nov','Dic'],
	        dayNames: ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
	        dayNamesShort: ['Dom','Lun','Mar','Mié','Juv','Vie','Sáb'],
	        dayNamesMin: ['Do','Lu','Ma','Mi','Ju','Vi','Sá'],
	        weekHeader: 'Sm',
	        dateFormat: 'dd/mm/yy',
	        firstDay: 1,
	        isRTL: false,
	        showMonthAfterYear: false,
	        yearSuffix: ''
	    };
	    $.datepicker.setDefaults($.datepicker.regional['es']);
	   
		    
		    
		    $("#fechaInicio, #fechaFin").datepicker({dateFormat: 'dd/mm/yy'});
			$("#fechaInicio, #fechaFin").val();
	
});
/*
$('#formProyecto').submit(function(event){
    // cancels the form submission
    event.preventDefault();
    // do whatever you want here
});
*/
function modificar()
{	
	var ok=true;	
	$('#fechaFin')[0].setCustomValidity("");
	$('#horas')[0].setCustomValidity("");
	
	ok=($('#nombre')[0].checkValidity());
	
	if (ok)
		ok=($('#fechaInicio')[0].checkValidity());
	if (ok)
		ok=($('#fechaFin')[0].checkValidity());
	if (ok){		
		ok=($('#horas')[0].checkValidity());		
	}
	
	<%if (ultimaFecha!=null){%>
	
	if (ok){
		fechaImputacion=new Date(<%=ultimaFecha.getTime()%>);
		
		from = $("#fechaFin").val().split("/");
		fechaNueva = new Date(from[2], from[1] - 1, from[0]);
		
		
		if (fechaImputacion.getTime()>fechaNueva.getTime()){
			$('#fechaFin')[0].setCustomValidity("La fecha de fin de proyecto no puede ser anterior a la última imputación (<%=formatoFecha.format(ultimaFecha)%>).");			
			return
		}	
		
	}
	
	<%}%>
	
	if (ok){		
		$("#jefe").prop('disabled', false);
		$("#codigo").prop('disabled', false);
		$("#fechaInicio").prop('disabled', false);		
		$("#formProyecto").submit();
	}
}



function volver()
{	 
	 $("#formProyecto").attr("action", "/gespro?accion=ini");		 
	 $("#formProyecto").submit();
}

</script>
