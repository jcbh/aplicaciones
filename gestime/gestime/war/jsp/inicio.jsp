<%
/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
 %>

<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE html>
<html lang="es">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../assets/ico/favicon.ico">

<title>Gestión del Tiempo en Proyectos</title>

<!-- Bootstrap core CSS -->
<link href="css/bootstrap.min.css" rel="stylesheet">

<!-- Gestime CSS -->
<link href="css/gestime.css" rel="stylesheet">




</head>

<body>	
	
    
     <jsp:include page="menu.jsp" ></jsp:include>
    
        <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container">
        <h2>Aplicaciones disponibles</h2>
        <p>Aquí tienes una lista de aplicaciones a las que tu usuario tiene acceso.</p>        
      </div>
    </div>
	
	<div class="container">
		
		<div class="row">							
			<div class="col-md-4">
				<h2>Registro de horas</h2>
				<p>Imputa lo que has estado haciendo</p>
				<p>
					<form action="gesinput" method="post">
            			<button type="submit" class="btn btn-default">Entrar &raquo;</button>
		          	</form>					
				</p>
			</div>
			<div class="col-md-8">				
				<h2>Visualización en Google Calendar</h2>
				<p>Revisa tu imputaciones</p>
				<p>
					<form action="gesinput" method="post">
						<input type="hidden" name="accion" id="accion" value="visua">
            			<button type="submit" class="btn btn-default">Entrar &raquo;</button>
		          	</form>					
				</p>
			</div>
			
			
			
							
		</div>
		
		<hr>
		
		<div class="row">
			<div class="col-md-4">
				<h2>Gestión de Proyectos</h2>
				<p>Administra los datos principales de tus proyectos</p>
				<p>
					<form action="gespro" method="post">
            			<button type="submit" class="btn btn-default">Entrar &raquo;</button>
		          	</form>
				</p>
			</div>
			<div class="col-md-4">
				<h2>Imputaciones en tus Proyectos</h2>
				<p>Revisa, aprueba o rechaza las horas imputadas en tus proyectos</p>
				<p>
					<form action="imputPro" method="post">
            			<button type="submit" class="btn btn-default">Entrar &raquo;</button>
		          	</form>					
				</p>
			</div>
			<div class="col-md-4">
				<h2>Informes de tus Proyectos</h2>
				<p>Obten estadísticas e informes</p>
				<p>
					<form action="informes" method="post">
            			<button type="submit" class="btn btn-default">Entrar &raquo;</button>
		          	</form>		
				</p>
			</div>
		</div>


   <hr>
        
        <div class="row">
        <div class="col-md-4">  </div>
			<div class="col-md-4">        
        		<img src="/img/time.jpg">        
      		</div>
      	</div>

		
	
		
		
		
      </div>
      <jsp:include page="pie.jsp" ></jsp:include>
	
		
	



	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
	<script src="../../dist/js/bootstrap.min.js"></script>
</body>
</html>