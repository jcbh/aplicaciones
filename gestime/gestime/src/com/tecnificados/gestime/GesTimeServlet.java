/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.appengine.api.utils.SystemProperty;
import com.google.apphosting.utils.remoteapi.RemoteApiPb.Request;

import java.util.logging.Logger;
import java.sql.*;

@SuppressWarnings("serial")
public class GesTimeServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(GesTimeServlet.class.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.info("Accedemos por post");
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String accion = "";
		String destino = "";

		log.info("Accedemos por get");

		UserService userService = UserServiceFactory.getUserService();

		User u = userService.getCurrentUser();

		String thisURL = req.getRequestURI();

		// Si est� autenticado
		if (req.getUserPrincipal() != null) {

			req.getSession(true).setAttribute("logOut", userService.createLogoutURL(thisURL));
			req.getSession(true).setAttribute("userMail", req.getUserPrincipal().getName());
			req.getSession(true).setAttribute("userName", u.getNickname());

			if ((req.getParameter("action") != null) && (!req.getParameter("action").equals(""))) {
				accion = req.getParameter("action");

				if (accion.equals("imputacion")) {
					destino = "gesinput?action=ini";
				}
			} else {
				log.info("destino por defecto: inicio.jsp");
				destino = "jsp/inicio.jsp";
			}
			// Si no est� autenticado
		} else {

			// req.getSession().setAttribute("urlGoogle",
			// userService.createLoginURL(thisURL));

			// destino="/jsp/ident.jsp";

			resp.sendRedirect(userService.createLoginURL("/gestime"));

		}

		if (destino.equals("")) {
			log.warning("No hay destino");
			destino = "/";
		}

		log.info("el destino es: " + destino);

		req.getRequestDispatcher(destino).forward(req, resp);

	}

	public boolean autenticacion(HttpServletRequest req, HttpServletResponse resp) {
		UserService userService = UserServiceFactory.getUserService();
		String thisURL = req.getRequestURI();

		boolean login = false;

		// Si esta logeado no hacemos nada
		if (req.getUserPrincipal() != null) {

			// resp.getWriter().println("<p>Hello, " +
			// req.getUserPrincipal().getName() + "!  You can <a href=\"" +
			// userService.createLogoutURL(thisURL)+
			// "\">sign out</a>.</p>");

			req.getSession(true).setAttribute("logOut", userService.createLogoutURL(thisURL));
			req.getSession(true).setAttribute("userName", req.getUserPrincipal().getName());

			login = true;

		} 

		return login;

	}

}
