/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Logger;

import com.tecnificados.gestime.bean.Proyecto;

public class ProyectoBD {

	private static final Logger log = Logger.getLogger(ProyectoBD.class.getName());

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");

	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet rset = null;

	private final String tabla = "proyectos";

	public synchronized void nuevo(Proyecto p) throws Exception {

		
			//INSERT INTO proyectos (nombre,jefe,fechaInicio,FechaFin,horas) values ("Reforma de la penintenciaria estatal de Fox River","Michael Scofield","10102010","12122016",2400);
		
			String insert = "insert into " + tabla + "(nombre,jefe,fechaInicio,FechaFin,horas) ";
			String values = "values ( ";

			
			
			if (p.getNombre() != null) {
				values += "\""+p.getNombre()+"\",";				
			}else{
				values += "\"\",";
			}

			if (p.getJefe() != null) {
				values += "\""+p.getJefe()+"\",";				
			}else{
				values += "\"\",";
			}
			
			if (p.getFechaInicio() != null) {
				values += "\""+dateFormat.format(p.getFechaInicio())+"\",";				
			}else{
				values += "\"\",";
			}
			
			if (p.getFechaFin() != null) {
				values += "\""+dateFormat.format(p.getFechaFin())+"\",";				
			}else{
				values += "\"\",";
			}
			
			if (p.getHoras() > 0) {
				values += p.getHoras()+"," ;
			}else{
				values += "0,";
			}
						
			values= values.substring(0,values.length()-1);
			
			values +=");";

			

			try {
				conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
				stmt = conn.createStatement();
				log.info("Insert: " + insert + values);
				stmt.execute(insert + values);

				} 
			catch (SQLException e) {
				log.warning("Error al insertar proyecto en bbdd: "+e.getMessage());
				e.printStackTrace();		
				throw new Exception("Error al crear proyecto en bbdd");
			} finally {
				try {
					if (rset != null)
						rset.close();
				} catch (Exception e) {
				}
				try {
					if (stmt != null)
						stmt.close();
				} catch (Exception e) {
				}
				try {
					if (conn != null)
						conn.close();
				} catch (Exception e) {
				}
			}

			
		
		
	}
	
	public synchronized void actualiza(Proyecto p) throws Exception {

		

		
		//UPDATE proyectos  SET id = "P000000001", nombre = "reforma de la penintenciaria estatal de Fox River", jefe = "Michael Scofield", fechaInicio = "10102010", fechaFin = "12122016", horas = 2400	WHERE id = "P000000001";
		
	
		String update = "update " + tabla + " ";
		String values = "SET id = \"" +p.getId()+"\", ";


		values += "nombre = \""+p.getNombre()+"\",";
		values += "jefe = \""+p.getJefe()+"\",";
		values += "fechaInicio = \""+dateFormat.format(p.getFechaInicio())+"\",";
		values += "fechaFin = \""+dateFormat.format(p.getFechaFin())+"\",";
		values += "horas= "+p.getHoras()+"," ;
		
					
		values= values.substring(0,values.length()-1);
		
		values +="	WHERE id = \""+p.getId()+"\"";

		

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Update: " + update + values);
			stmt.execute(update + values);

			} 
		catch (SQLException e) {
			log.warning("Error al modificar proyecto en bbdd: "+e.getMessage());
			e.printStackTrace();		
			throw new Exception("Error al modificar proyecto en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		
	
	
}

	public synchronized ArrayList<Proyecto> busqueda() throws Exception {
	
		return busqueda(new Proyecto());
	}
	
	public synchronized ArrayList<Proyecto> busqueda(Proyecto p) throws Exception {
		String query = "select * from " + tabla;
		String conditions = "";

		if (p.getFechaFin() != null) {
			conditions += " and fechaFin like '" + dateFormat.format(p.getFechaFin()) + "' ";
		}

		if (p.getFechaInicio() != null) {
			conditions += " and fechaInicio like '" + dateFormat.format(p.getFechaInicio()) + "' ";
		}

		if (p.getHoras() != 0) {
			conditions += " and horas = " + p.getHoras() + " ";
		}

		if (!p.getId().equals("")) {
			conditions += " and id = " + p.getId() + " ";
		}

		if (!p.getJefe().equals("")) {
			conditions += " and jefe like '" + p.getJefe() + "' ";
		}

		if (!p.getNombre().equals("")) {
			conditions += " and nombre like '" + p.getNombre() + "' ";
		}

		if (!conditions.equals(""))
			conditions = conditions.replaceFirst("and", "where");
		
		conditions += " order by nombre;";

		ArrayList<Proyecto> listado = new ArrayList<Proyecto>();

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Query: " + query + conditions);
			rset = stmt.executeQuery(query + conditions);

			while (rset.next()) {

				Proyecto proyecto = new Proyecto();
				proyecto.setFechaFin(dateFormat.parse(rset.getString("fechaFin")));
				proyecto.setFechaInicio(dateFormat.parse(rset.getString("fechaInicio")));
				proyecto.setHoras(rset.getFloat("horas"));
				proyecto.setId(rset.getString("id"));
				proyecto.setJefe(rset.getString("jefe"));
				proyecto.setNombre(rset.getString("nombre"));
				
				listado.add(proyecto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("Error al buscar proyecto en bbdd");
		} catch (ParseException e) {
			e.printStackTrace();
			throw new Exception("Error al buscar proyecto en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		return listado;
	}
	
	public synchronized ArrayList<Proyecto> busqueda(Proyecto p, String orden) throws Exception {
		String query = "select * from " + tabla;
		String conditions = "";

		if (p.getFechaFin() != null) {
			conditions += " and fechaFin like '" + dateFormat.format(p.getFechaFin()) + "' ";
		}

		if (p.getFechaInicio() != null) {
			conditions += " and fechaInicio like '" + dateFormat.format(p.getFechaInicio()) + "' ";
		}

		if (p.getHoras() != 0) {
			conditions += " and horas = " + p.getHoras() + " ";
		}

		if (!p.getId().equals("")) {
			conditions += " and id like " + p.getId() + " ";
		}

		if (!p.getJefe().equals("")) {
			conditions += " and jefe like '" + p.getJefe() + "' ";
		}

		if (!p.getNombre().equals("")) {
			conditions += " and nombre like '" + p.getNombre() + "' ";
		}

		if (!conditions.equals(""))
			conditions = conditions.replaceFirst("and", "where");
		

		ArrayList<Proyecto> listado = new ArrayList<Proyecto>();

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Query: " + query + conditions);
			

			if (orden.equals("")){			
				log.info("Query: " + query + conditions+ ";");
				rset = stmt.executeQuery(query + conditions+ ";");
			}else{
				log.info("Query: " + query + conditions + " "+orden+ ";");
				rset = stmt.executeQuery(query + conditions + " "+orden+ ";");
			}
			
			while (rset.next()) {

				Proyecto proyecto = new Proyecto();
				proyecto.setFechaFin(dateFormat.parse(rset.getString("fechaFin")));
				proyecto.setFechaInicio(dateFormat.parse(rset.getString("fechaInicio")));
				proyecto.setHoras(rset.getFloat("horas"));
				proyecto.setId(rset.getString("id"));
				proyecto.setJefe(rset.getString("jefe"));
				proyecto.setNombre(rset.getString("nombre"));
				
				listado.add(proyecto);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("Error al modificar proyecto en bbdd");
		} catch (ParseException e) {
			e.printStackTrace();
			throw new Exception("Error al modificar proyecto en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		return listado;
	}

	public synchronized Proyecto busqueda(String id) throws Exception {
		Proyecto p = new Proyecto();
		p.setId(id);
		ArrayList<Proyecto> lista=busqueda(p);
		return lista.get(0);
	}
	
	
	public synchronized void borra(String id) throws Exception{
	
		
		String delete="delete from proyectos where id like '"+id+"'";
		
		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("delete: " + delete);
			stmt.execute(delete);

			} 
		catch (SQLException e) {
			log.warning("Error al borrar proyecto en bbdd: "+e.getMessage());
			e.printStackTrace();		
			throw new Exception("Error al borrar proyecto en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		
	}
	
	
	public synchronized int cuenta (Proyecto p) throws Exception {
		
		int cuenta=0;
		
		String query = "select count(*) from " + tabla;
		String conditions = "";

		if (p.getFechaFin() != null) {
			conditions += " and fechaFin like '" + dateFormat.format(p.getFechaFin()) + "' ";
		}

		if (p.getFechaInicio() != null) {
			conditions += " and fechaInicio like '" + dateFormat.format(p.getFechaInicio()) + "' ";
		}

		if (p.getHoras() != 0) {
			conditions += " and horas = " + p.getHoras() + "' ";
		}

		if (!p.getId().equals("")) {
			conditions += " and id like '" + p.getId() + "' ";
		}

		if (!p.getJefe().equals("")) {
			conditions += " and jefe like '" + p.getJefe() + "' ";
		}

		if (!p.getNombre().equals("")) {
			conditions += " and nombre like '" + p.getNombre() + "' ";
		}

		if (!conditions.equals(""))
			conditions = conditions.replaceFirst("and", "where");
		conditions += ";";



		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Query: " + query + conditions);
			rset = stmt.executeQuery(query + conditions);

			if (rset.next()) {

				
				
				cuenta=rset.getInt(1);

			}
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
				throw new Exception("Error al contar proyecto en bbdd");
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		return cuenta;
	}
	

	public static void main(String[] args) throws ParseException {

		
		/*
		try {
			pool.setupDriver("jdbc:mysql://127.0.0.1:3306/gestime");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ProyectoBD pBD = new ProyectoBD();

		Proyecto p=new Proyecto();
		
		p.setId("P00000005");
		p.setFechaInicio(ProyectoBD.dateFormat.parse("25122004"));
		p.setFechaFin(ProyectoBD.dateFormat.parse("25122016"));
		p.setHoras(200);
		p.setJefe("Topo");
		p.setNombre("Excavación de tuneles en el Asteroide B612");
		
		pBD.nuevo(p);
		
		ArrayList<Proyecto> a = pBD.busqueda(new Proyecto());

		for (Proyecto pr : a) {
			System.out.println(pr.toString());
			
		}
		
		try {
			pool.shutdownDriver();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		*/
		

	    


	}

}
