/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */
package com.tecnificados.gestime.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;
import java.util.logging.Logger;

import com.tecnificados.gestime.bean.Imputacion;
import com.tecnificados.gestime.bean.Proyecto;

public class ImputacionBD {

	private static final Logger log = Logger.getLogger(ImputacionBD.class.getName());

	public static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

	private Connection conn = null;
	private Statement stmt = null;
	private ResultSet rset = null;

	private final String tabla = "imputaciones";

	

	public synchronized void nuevo(Imputacion i) throws Exception {

		// INSERT INTO imputaciones( proyecto, horas, usuario, fecha, estado)
		// values (P000000001",8.5,jcballesteros,"06/05/2014",0);

		
		String insert = "insert into " + tabla + "(proyecto, horas, usuario, fecha, estado, descripcion) ";
		String values = "values ( ";

	
		if (i.getProyecto() != null) {
			values += "\"" + i.getProyecto() + "\",";
		} else {
			values += "\"\",";
		}

		if (i.getHoras() > 0) {
			values += i.getHoras() + ",";
		} else {
			values += "0,";
		}

		if (i.getUsuario() != null) {
			values += "\"" + i.getUsuario() + "\",";
		} else {
			values += "\"\",";
		}

		if (i.getFecha() != null) {
			values += "\"" + dateFormat.format(i.getFecha()) + "\",";
		} else {
			values += "\"\",";
		}

		if (i.getEstado() >= 0) {
			values += i.getEstado() + ",";
		} else {
			values += "-1,";
		}
		
		if (i.getDescripcion() != null) {
			values += "\"" + i.getDescripcion() + "\",";
		} else {
			values += "\"\",";
		}

		values = values.substring(0, values.length() - 1);

		values += ");";

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Insert: " + insert + values);
			stmt.execute(insert + values);

		} catch (SQLException e) {
			log.warning("Error al insertar imputacion en bbdd: " + e.getMessage());
			e.printStackTrace();
			throw new Exception("Error al insertar imputacion en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

	}

	public synchronized Imputacion busqueda(int id) throws Exception {
		Imputacion i = new Imputacion();
		i.setId(id);
		
		ArrayList<Imputacion> lista = busqueda(i);
		
		return lista.get(0);
	}

	public synchronized ArrayList<Imputacion> busqueda() throws Exception {

		return busqueda(new Imputacion());
	}

	
	public synchronized ArrayList<Imputacion> busqueda(Imputacion i) throws Exception {
		return busqueda(i, "");
	}
	
	public synchronized ArrayList<Imputacion> busqueda(Imputacion i, String orden) throws Exception {
		String query = "select * from " + tabla;
		String conditions = "";
		

		if (i.getEstado() != -1) {
			conditions += " and estado = " + (i.getEstado()) + " ";
		}

		if (i.getFecha() != null) {
			conditions += " and fecha like '" + dateFormat.format(i.getFecha()) + "' ";
		}

		if (i.getHoras() != 0) {
			conditions += " and horas = " + i.getHoras() + " ";
		}

		if (!i.getProyecto().equals("")) {
			conditions += " and proyecto like '" + i.getProyecto() + "' ";
		}

		if (!i.getUsuario().equals("")) {
			conditions += " and usuario like '" + i.getUsuario() + "' ";
		}

		if (i.getId()!=-1) {
			conditions += " and id = " + i.getId() + " ";
		}
		
		if (!i.getDescripcion().equals("")) {
			conditions += " and descripcion like '" + i.getDescripcion() + "' ";
		}
		

		if (!conditions.equals(""))
			conditions = conditions.replaceFirst("and", "where");
		

		ArrayList<Imputacion> listado = new ArrayList<Imputacion>();

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			
			if (orden.equals("")){			
				log.info("Query: " + query + conditions+ ";");
				rset = stmt.executeQuery(query + conditions+ ";");
			}else{
				log.info("Query: " + query + conditions + " "+orden+ ";");
				rset = stmt.executeQuery(query + conditions + " "+orden+ ";");
			}

			while (rset.next()) {

				Imputacion inputacion = new Imputacion();
				inputacion.setId(rset.getInt("id"));
				inputacion.setFecha(dateFormat.parse(rset.getString("fecha")));
				inputacion.setHoras(rset.getFloat("horas"));
				inputacion.setUsuario(rset.getString("usuario"));
				inputacion.setProyecto(rset.getString("proyecto"));
				inputacion.setEstado(rset.getInt("estado"));
				inputacion.setDescripcion(rset.getString("descripcion"));

				listado.add(inputacion);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("Error al buscar imputacion en bbdd");
		} catch (ParseException e) {
			e.printStackTrace();
			throw new Exception("Error al buscar imputacion en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		return listado;
	}

	public synchronized int cuenta(Imputacion i) throws Exception {
		String query = "select count(*) from " + tabla;
		String conditions = "";

		int count = 0;

		if (i.getEstado() != -1) {
			conditions += " and estado = " + (i.getEstado()) + " ";
		}

		if (i.getFecha() != null) {
			conditions += " and fecha like '" + dateFormat.format(i.getFecha()) + "' ";
		}

		if (i.getHoras() != 0) {
			conditions += " and horas = " + i.getHoras() + "' ";
		}

		if (!i.getProyecto().equals("")) {
			conditions += " and proyecto like '" + i.getProyecto() + "' ";
		}

		if (!i.getUsuario().equals("")) {
			conditions += " and usuario like '" + i.getUsuario() + "' ";
		}

		if (i.getId()!=-1) {
			conditions += " and id = " + i.getId() + " ";
		}
		
		if (!i.getDescripcion().equals("")) {
			conditions += " and descripcion like '" + i.getDescripcion() + "' ";
		}

		if (!conditions.equals(""))
			conditions = conditions.replaceFirst("and", "where");
		conditions += ";";

		ArrayList<Imputacion> listado = new ArrayList<Imputacion>();

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Query: " + query + conditions);
			rset = stmt.executeQuery(query + conditions);

			if (rset.next()) {

				count = rset.getInt(1);

			}
		} catch (SQLException e) {
			e.printStackTrace();
			throw new Exception("Error al contar imputacion en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		return count;
	}
	
	
	public synchronized void actualiza(Imputacion i) throws Exception {

		

		

		//UPDATE imputaciones SET id = "id",proyecto = "proyecto",horas = horas,usuario = "usuario",fecha = "fecha",estado = "estado", descripcion = "descripcion" WHERE id like "expr";
		
		
	
		String update = "update " + tabla + " ";
		String values = "SET id = \"" +i.getId()+"\", ";


		values += "proyecto = \""+i.getProyecto()+"\",";
		values += "horas = "+i.getHoras()+",";
		values += "usuario = \""+i.getUsuario()+"\",";
		values += "fecha = \""+dateFormat.format(i.getFecha())+"\",";
		values += "estado= "+i.getEstado()+"," ;
		values += "descripcion= \""+i.getDescripcion()+"\"," ;
		
					
		values= values.substring(0,values.length()-1);
		
		values +="	WHERE id = \""+i.getId()+"\"";

		

		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("Update: " + update + values);
			stmt.execute(update + values);

			} 
		catch (SQLException e) {
			log.warning("Error al modificar imputacion en bbdd: "+e.getMessage());
			e.printStackTrace();			
			throw new Exception("Error al modificar imputacion en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}

		
	
	
}


	/*
	 * public synchronized void borra(String id){
	 * 
	 * 
	 * String delete="delete from proyectos where id like '"+id+"'";
	 * 
	 * try { conn =
	 * DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime"); stmt
	 * = conn.createStatement(); log.info("delete: " + delete);
	 * stmt.execute(delete);
	 * 
	 * } catch (SQLException e) {
	 * log.warning("Error al borrar proyecto en bbdd: "+e.getMessage());
	 * e.printStackTrace(); } finally { try { if (rset != null) rset.close(); }
	 * catch (Exception e) { } try { if (stmt != null) stmt.close(); } catch
	 * (Exception e) { } try { if (conn != null) conn.close(); } catch
	 * (Exception e) { } }
	 * 
	 * }
	 */
	
	
	public synchronized void borra(String id) throws Exception{
	
		
		String delete="delete from imputaciones where id like '"+id+"'";
		

		
		try {
			conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
			stmt = conn.createStatement();
			log.info("delete: " + delete);
			stmt.execute(delete);

			} 
		catch (SQLException e) {
			log.warning("Error al borrar imputacion en bbdd: "+e.getMessage());
			e.printStackTrace();	
			throw new Exception("Error al borrar imputacion en bbdd");
		} finally {
			try {
				if (rset != null)
					rset.close();
			} catch (Exception e) {
			}
			try {
				if (stmt != null)
					stmt.close();
			} catch (Exception e) {
			}
			try {
				if (conn != null)
					conn.close();
			} catch (Exception e) {
			}
		}
		
	}
	
	public static void main(String[] args) throws ParseException {

		/*
		try {
			pool.setupDriver("jdbc:mysql://127.0.0.1:3306/gestime");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ImputacionBD iBD = new ImputacionBD();

		Imputacion i = new Imputacion();

		i.setId("I00000008");
		i.setFecha(ProyectoBD.dateFormat.parse("25122004"));
		i.setHoras(200);
		i.setEstado(0);
		i.setUsuario("jballest");
		i.setProyecto("P000001");

		iBD.nuevo(i, true);

		ArrayList<Imputacion> a = iBD.busqueda(new Imputacion());

		for (Imputacion pr : a) {
			System.out.println(pr.toString());

		}

		try {
			pool.shutdownDriver();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
*/
	}
	

}
