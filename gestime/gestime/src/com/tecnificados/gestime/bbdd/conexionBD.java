/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import com.google.appengine.api.utils.SystemProperty;

public class conexionBD {

	public void TestConexion(){
		String url = null;
	    try {
	      if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {
	        // Load the class that provides the new "jdbc:google:mysql://" prefix.
	        Class.forName("com.mysql.jdbc.GoogleDriver");
	        url = "jdbc:google:mysql://tecnificados:gestime/guestbook?user=root";
	      } else {
	        // Local MySQL instance to use during development.
	        Class.forName("com.mysql.jdbc.Driver");
	    	  
	    	  
	        url = "jdbc:mysql://127.0.0.1:3306/guestbook";       
	        

	        // Alternatively, connect to a Google Cloud SQL instance using:
	        // jdbc:mysql://ip-address-of-google-cloud-sql-instance:3306/guestbook?user=root
	        
	      }
	    } catch (Exception e) {
	      e.printStackTrace();
	      return;
	    }
	    
	    
	    try {
	      Connection conn = DriverManager.getConnection(url,"root","Predator");
	      try {
	        String fname = "Carlos";
	        String content = "Saludo";
	        
	          String statement = "INSERT INTO entries (guestName, content) VALUES( ? , ? )";
	          PreparedStatement stmt = conn.prepareStatement(statement);
	          stmt.setString(1, fname);
	          stmt.setString(2, content);
	          int success = 2;
	          success = stmt.executeUpdate();
	        
	        
	      } finally {
	        conn.close();
	      }
	    } catch (SQLException e) {
	      e.printStackTrace();
	    }


	}
	
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
