/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime.bbdd;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Consulta {

	public static void test()
	{
		   Connection conn = null;
	        Statement stmt = null;
	        ResultSet rset = null;

	        try {
	            System.out.println("Creating connection.");
	            conn = DriverManager.getConnection("jdbc:apache:commons:dbcp:poolGestime");
	            System.out.println("Creating statement.");
	            stmt = conn.createStatement();
	            System.out.println("Executing statement.");
	            //rset = stmt.executeQuery(args[1]);
	            rset = stmt.executeQuery("select * from proyectos;");            
	            System.out.println("Results:");
	            int numcols = rset.getMetaData().getColumnCount();
	            while(rset.next()) {
	                for(int i=1;i<=numcols;i++) {
	                    System.out.print("\t" + rset.getString(i));
	                }
	                System.out.println("");
	            }
	        } catch(SQLException e) {
	            e.printStackTrace();
	        } finally {
	            try { if (rset != null) rset.close(); } catch(Exception e) { }
	            try { if (stmt != null) stmt.close(); } catch(Exception e) { }
	            try { if (conn != null) conn.close(); } catch(Exception e) { }
	        }
	}
	
}
