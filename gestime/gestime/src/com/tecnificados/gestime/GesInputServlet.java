/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.appengine.auth.oauth2.AbstractAppEngineAuthorizationCodeServlet;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.apphosting.utils.remoteapi.RemoteApiPb.Request;
import com.tecnificados.gestime.bbdd.ImputacionBD;
import com.tecnificados.gestime.bbdd.ProyectoBD;
import com.tecnificados.gestime.bean.EventoGC;
import com.tecnificados.gestime.bean.Imputacion;
import com.tecnificados.gestime.bean.Proyecto;

import java.util.logging.Logger;

@SuppressWarnings("serial")
public class GesInputServlet extends AbstractAppEngineAuthorizationCodeServlet {

	private static final String APPLICATION_NAME = "tecnificados";

	private static final long serialVersionUID = 1L;

	private static ProyectoBD proyectoBD = new ProyectoBD();
	private static ImputacionBD imputacionBD = new ImputacionBD();

	private Calendar cliente = null;

	SimpleDateFormat formatoEntrada = new SimpleDateFormat("dd/MM/yyyy");

	private static final Logger log = Logger.getLogger(GesInputServlet.class.getName());

	private final String _nombreCalendarioDefecto = "Gestion de Tiempo y Proyectos";
	private final String _timeZoneDefecto = "Europe/Madrid";

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.info("Accedemos por post");
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		String idCalendario = null;

		String accion = "";
		String destino = "";

		log.info("Accedemos por get");

		UserService userService = UserServiceFactory.getUserService();
		User u = userService.getCurrentUser();

		if (req.getUserPrincipal() != null) {

			req.getSession(true).setAttribute("logOut", userService.createLogoutURL("/gestime"));
			req.getSession(true).setAttribute("userMail", req.getUserPrincipal().getName());
			req.getSession(true).setAttribute("userName", u.getNickname());
		}

		com.google.api.services.calendar.model.CalendarList feed = null;

		if ((req.getParameter("accion") != null) && (!req.getParameter("accion").equals(""))) {
			accion = req.getParameter("accion");
		} else {
			accion = "ini";
		}

		log.info("La acci�n es: " + accion);

		if (accion.equals("ini")) {

			try {
				// Get the stored credentials using the Authorization Flow
				AuthorizationCodeFlow authFlow = initializeFlow();
				Credential credential = authFlow.loadCredential(getUserId(req));
				// Build the Calendar object using the credentials
				cliente = new Calendar.Builder(Utils.HTTP_TRANSPORT, Utils.JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();
				feed = cliente.calendarList().list().execute();
			} catch (Exception e) {
				e.printStackTrace();
				log.warning(e.toString());
			}
			String rSeleccion="";
			if (req.getParameter("rSeleccion")!=null)
				rSeleccion=req.getParameter("rSeleccion");
			
			String parametros = "";
			if (req.getParameter("errorBD") != null) {
				parametros += "?errorBD=true";
			}

			if (req.getParameter("errorGC") != null) {
				parametros += "?errorGC=true";
			}

			if (req.getParameter("errorParseo") != null) {
				parametros += "?errorParseo=true";
			}
			
			

			if (parametros.equals("")) {
				List<CalendarListEntry> listadoCalendarios = feed.getItems();

				for (CalendarListEntry c : listadoCalendarios) {
					if (c.getSummary().equals(_nombreCalendarioDefecto)) {
						idCalendario = c.getId();

						break;
					}

				}

				if (idCalendario == null) {
					// Nuevo calendario
					com.google.api.services.calendar.model.Calendar calendario = new com.google.api.services.calendar.model.Calendar();

					calendario.setSummary(_nombreCalendarioDefecto);
					calendario.setTimeZone(_timeZoneDefecto);

					calendario = cliente.calendars().insert(calendario).execute();

					idCalendario = calendario.getId();

				}

				String eventosJson = listadoImputacionesGoogleCalendar(idCalendario);
				req.getSession(true).setAttribute("eventos", eventosJson);

				ArrayList<Imputacion> listaImputaciones;
				try {
					listaImputaciones = listadoImputacionesBBDD(u.getNickname());
					req.getSession(true).setAttribute("imputaciones", listaImputaciones);
					req.getSession(true).setAttribute("proyectos", proyectoBD.busqueda());
				} catch (Exception e) {
					log.warning("Error al escribir al parsear fecha: " + e.getMessage());
					e.printStackTrace();
					destino = "/jsp/horas.jsp?errorBD=true";
				}

				

				req.getSession(true).setAttribute("idCalendario", idCalendario);
			}

			if (parametros.equals("")&&(!rSeleccion.equals("")))
				parametros="?rSeleccion="+rSeleccion;
			
			if (destino.equals(""))
				destino = "/jsp/horas.jsp" + parametros;

		} else if (accion.equals("alta")) {

			String rSeleccion="";
			
			if (req.getParameter("seleccionFechas")!=null)
				rSeleccion=req.getParameter("seleccionFechas");
			
			String descripcion = req.getParameter("descripcion");
			String proyecto = req.getParameter("proyecto");
			String proyectoId = req.getParameter("proyectoId");
			String dia = req.getParameter("dia");
			String horaI = req.getParameter("horaI");
			String horaF = req.getParameter("horaF");

			String horas = req.getParameter("horas");
			String usuario = u.getNickname();

			idCalendario = req.getParameter("idCalendario");

			Date fechaEntrada = null;
			try {
				fechaEntrada = formatoEntrada.parse(dia);
			} catch (ParseException e) {
				log.warning("Error al escribir al parsear fecha: " + e.getMessage());
				e.printStackTrace();
				destino = "/gesinput?accion=ini&errorParseo=true";
			}

			if (destino.equals("")) {
				Imputacion i = new Imputacion();
				i.setEstado(0);
				i.setProyecto(proyecto);
				i.setFecha(fechaEntrada);
				i.setHoras(Float.parseFloat(horas));
				i.setUsuario(usuario);
				i.setDescripcion(descripcion);

				// Buscamos si hay registro iguales
				ArrayList<Imputacion> busqueda = null;
				try {
					busqueda = imputacionBD.busqueda(i);
				} catch (Exception e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
					log.warning("Error al escribir al buscar imputaciones");
					destino = "/gesinput?accion=ini&errorBD=true";
				}

				// Ahora insertamos en BBDD y en google si no los hay
				if ((destino.equals("")) && (busqueda.size() == 0)) {

					Date fechaIni = new Date();
					Date fechaFin = new Date();

					SimpleDateFormat formatoDelTexto = new SimpleDateFormat("dd/MM/yyyy HH:mm");

					try {

						fechaIni = formatoDelTexto.parse(dia + " " + horaI);
						fechaFin = formatoDelTexto.parse(dia + " " + horaF);

					} catch (ParseException ex) {

						log.warning("Error al escribir al parsear fechas: " + ex.getMessage());
						ex.printStackTrace();
						destino = "/gesinput?accion=ini&errorParseo=true";

					}

					String idEvento = "";
					if (destino.equals("")) {

						Event event = new Event();
						event.setSummary(proyectoId + " (" + proyecto + ")");
						event.setDescription(descripcion);

						DateTime start = new DateTime(fechaIni, TimeZone.getTimeZone("UTC"));
						DateTime end = new DateTime(fechaFin, TimeZone.getTimeZone("UTC"));

						event.setStart(new EventDateTime().setDateTime(start));
						event.setEnd(new EventDateTime().setDateTime(end));

						Event result = cliente.events().insert(idCalendario, event).execute();

						if ((result != null) && (result.get("status") != null) && (result.get("status").equals("confirmed"))) {
							log.info("Escritura en google calendar correcta");
							idEvento = (String) result.get("id");
						} else {
							log.warning("Error al escribir en google calendar");
							destino = "/gesinput?accion=ini&errorGC=true";
						}
					}
					if (destino.equals("")) {
						try {
							imputacionBD.nuevo(i);
						} catch (Exception e) {

							// Intento deshacer la escritura en google calendar
							if (!idEvento.equals(""))
								cliente.events().delete(idCalendario, idEvento).execute();

							log.warning("Error al escribir en Base de datos: " + e.getMessage());
							destino = "/gesinput?accion=ini&errorBD=true";
						}
					}

				}
			}
			
			if (destino.equals(""))
				destino = "/gesinput?accion=ini&rSeleccion="+rSeleccion;

		} else if (accion.equals("listadoImputaciones")) {

			String eventosJson = listadoImputacionesGoogleCalendar("");

			req.getSession(true).setAttribute("eventos", eventosJson);

			resp.setContentType("application/json");

			PrintWriter out = resp.getWriter();
			out.print(eventosJson);

			destino = null;
		} else if (accion.equals("baja")) {

			
			String rSeleccion="";
			
			if (req.getParameter("rSeleccion")!=null)
				rSeleccion=req.getParameter("rSeleccion");
			
			
			String paramIds = req.getParameter("imputaciones");
			idCalendario = req.getParameter("idCalendario");

			if (paramIds != null) {

				if (paramIds.indexOf("||") > 0) {

					String[] arrayIds = paramIds.split("\\|\\|");

					String idGoogle = arrayIds[0];
					String idBBDD = arrayIds[1];

					try {
						imputacionBD.borra(idBBDD);
					} catch (Exception e) {
						log.warning("Error al escribir en Base de datos: " + e.getMessage());
						destino = "/gesinput?accion=ini&errorBD=true";
					}

					if (destino.equals("")) {
						try {
							cliente.events().delete(idCalendario, idGoogle).execute();
						} catch (Exception e) {
							log.warning("Error al escribir en Google Calendar: " + e.getMessage());
							destino = "/gesinput?accion=ini&errorGC=true";
						}
					}

				}
			}

			if (destino.equals("")) {
				destino = "/gesinput?accion=ini&rSeleccion="+rSeleccion;
			}

		} else if (accion.equals("visua")) {
			try {
				// Get the stored credentials using the Authorization Flow
				AuthorizationCodeFlow authFlow = initializeFlow();
				Credential credential = authFlow.loadCredential(getUserId(req));
				// Build the Calendar object using the credentials
				cliente = new Calendar.Builder(Utils.HTTP_TRANSPORT, Utils.JSON_FACTORY, credential).setApplicationName(APPLICATION_NAME).build();

			} catch (Exception e) {
				e.printStackTrace();
				log.warning(e.toString());
			}

			feed = cliente.calendarList().list().execute();

			List<CalendarListEntry> listadoCalendarios = feed.getItems();

			for (CalendarListEntry c : listadoCalendarios) {
				if (c.getSummary().equals(_nombreCalendarioDefecto)) {
					idCalendario = c.getId();
					break;
				}

			}

			;

			if (idCalendario == null) {
				// Nuevo calendario
				com.google.api.services.calendar.model.Calendar calendario = new com.google.api.services.calendar.model.Calendar();

				calendario.setSummary(_nombreCalendarioDefecto);
				calendario.setTimeZone(_timeZoneDefecto);

				calendario = cliente.calendars().insert(calendario).execute();

				idCalendario = calendario.getId();

			}

			req.getSession(true).setAttribute("idCalendario", idCalendario);

			destino = "/jsp/calendar.jsp";
		}

		if (destino != null)
			req.getRequestDispatcher(destino).forward(req, resp);

	}

	private String listadoImputacionesGoogleCalendar(String id) throws IOException {
		// Busqueda de eventos
		Events feed2 = cliente.events().list(id).execute();

		List<Event> listadoEventosCalendar = feed2.getItems();

		List<EventoGC> listadoEventosProcesados = new ArrayList<EventoGC>();

		for (Event e : listadoEventosCalendar) {

			EventoGC eventoTemp = new EventoGC();
			EventDateTime t = e.getStart();
			Date temp = new Date(t.getDateTime().getValue());
			eventoTemp.setFechaInicial(temp);
			t = e.getEnd();
			temp = new Date(t.getDateTime().getValue());
			eventoTemp.setFechaFin(temp);
			eventoTemp.setProyecto(StringEscapeUtils.escapeHtml(e.getSummary()));

			eventoTemp.setDescripcion(StringEscapeUtils.escapeHtml(e.getDescription()));
			eventoTemp.setId(e.getId());

			listadoEventosProcesados.add(eventoTemp);
		}

		Collections.sort(listadoEventosProcesados);

		String eventosJson = "";
		for (EventoGC e : listadoEventosProcesados) {
			eventosJson += e.toJson() + ",";
		}
		if (eventosJson.length() > 0)
			eventosJson = eventosJson.substring(0, eventosJson.length() - 1);

		eventosJson = "{\"eventos\": [" + eventosJson + "]}";
		return eventosJson;
	}

	private ArrayList<Imputacion> listadoImputacionesBBDD(String usuario) throws Exception {

		Imputacion i = new Imputacion();
		i.setUsuario(usuario);

		ArrayList<Imputacion> lista = imputacionBD.busqueda(i, "order by fecha asc");

		return lista;

	}

	@Override
	protected AuthorizationCodeFlow initializeFlow() throws ServletException, IOException {
		return Utils.initializeFlow();
	}

	@Override
	protected String getRedirectUri(HttpServletRequest req) throws ServletException, IOException {
		return Utils.getRedirectUri(req);
	}

}
