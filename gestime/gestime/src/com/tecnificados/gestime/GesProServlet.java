/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.appengine.auth.oauth2.AbstractAppEngineAuthorizationCodeServlet;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.apphosting.utils.remoteapi.RemoteApiPb.Request;
import com.tecnificados.gestime.bbdd.ImputacionBD;
import com.tecnificados.gestime.bbdd.ProyectoBD;
import com.tecnificados.gestime.bean.Imputacion;
import com.tecnificados.gestime.bean.Proyecto;

import java.util.logging.Logger;

@SuppressWarnings("serial")
public class GesProServlet extends AbstractAppEngineAuthorizationCodeServlet {

	private static final long serialVersionUID = 1L;

	private static ProyectoBD proyectoBD = new ProyectoBD();
	private static ImputacionBD imputacionBD = new ImputacionBD();

	private static Random randomGenerator = new Random();

	private static final Logger log = Logger.getLogger(GesProServlet.class.getName());
	List<String> errores = new ArrayList<String>();
	String accion = "";
	String destino = "";

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.info("Accedemos por post");
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.info("Accedemos por get");

		accion = "";

		String destino = "";

		log.info("Accedemos por get");

		UserService userService = UserServiceFactory.getUserService();
		User u = userService.getCurrentUser();

		// Si est� autenticado
		if (req.getUserPrincipal() != null) {

			req.getSession(true).setAttribute("logOut", userService.createLogoutURL("/gestime"));
			req.getSession(true).setAttribute("userMail", req.getUserPrincipal().getName());
			req.getSession(true).setAttribute("userName", u.getNickname());

		} else {
			resp.sendRedirect(userService.createLoginURL("/gestime"));
			return;
		}

		if ((req.getParameter("accion") != null) && (!req.getParameter("accion").equals(""))) {
			accion = req.getParameter("accion");
		} else {
			accion = "ini";
		}

		log.info("La acci�n es: " + accion);

		if (accion.equals("ini")) {

			String parametros = "";
			if (req.getParameter("errorBD") != null) {
				parametros += "?errorBD=true";
			}

			if (req.getParameter("errorParseo") != null) {
				parametros += "?errorParseo=true";
			}

			if (parametros.equals("")) {
				try {
					Proyecto p = new Proyecto();
					p.setJefe(u.getNickname());

					ArrayList<Proyecto> proyectos = proyectoBD.busqueda(p);

					Map<String, String> mapaProyectoHoras = new HashMap<String, String>();

					// Recorro todos los proyectos para calcular el n�mero de
					// horas
					// imputadas
					for (Proyecto proyectoTemp : proyectos) {
						Imputacion i = new Imputacion();
						i.setProyecto(proyectoTemp.getId());

						ArrayList<Imputacion> inputacionesProyecto = imputacionBD.busqueda(i);
						float horas = 0;
						for (Imputacion inputacionTemporal : inputacionesProyecto) {
							horas += inputacionTemporal.getHoras();
						}
						mapaProyectoHoras.put(proyectoTemp.getId(), String.valueOf(horas));
					}

					// Introduzco en la session los proyecto que tiene este
					// usuario
					req.getSession(true).setAttribute("proyectos", proyectos);
					// Introduzco en el mapa de horas y proyectos
					req.getSession(true).setAttribute("mapa", mapaProyectoHoras);

				} catch (Exception e) {
					destino = "/gespro?accion=ini&errorBD=true";
				}
			}

			if (destino.equals(""))
				destino = "/jsp/proyectos.jsp"+parametros;

		} else if (accion.equals("alta")) {

			String nombre = req.getParameter("nombre");
			String fechaInicio = req.getParameter("fechaInicio");
			String fechaFin = req.getParameter("fechaFin");
			String jefe = req.getParameter("jefe");
			String horas = req.getParameter("horas");

			Proyecto nProyecto = new Proyecto();

			fechaFin = fechaFin.replaceAll("\\/", "");
			fechaInicio = fechaInicio.replaceAll("\\/", "");

			try {
				nProyecto.setFechaFin(ProyectoBD.dateFormat.parse(fechaFin));
				nProyecto.setFechaInicio(ProyectoBD.dateFormat.parse(fechaInicio));
				nProyecto.setNombre(nombre);
				nProyecto.setJefe(jefe);
				nProyecto.setHoras(Float.parseFloat(horas));

				try {
					ArrayList<Proyecto> busqueda = proyectoBD.busqueda(nProyecto);
					if (busqueda.size() == 0)
						proyectoBD.nuevo(nProyecto);
				} catch (Exception e) {
					destino = "/gespro?accion=ini&errorBD=true";
				}

			} catch (ParseException e) {
				log.warning("Error al parsear fecha");
				e.printStackTrace();
			}

			if (destino.equals(""))
				destino = "/gespro?accion=ini";

		} else if (accion.equals("baja")) {

			String codigo = req.getParameter("codigo");

			try {
				proyectoBD.borra(codigo);
			} catch (Exception e) {
				destino = "/gespro?accion=ini&errorBD=true";
			}

			if (destino.equals(""))
				destino = "/gespro?accion=ini";

		} else if (accion.equals("editar")) {

			String codigo = req.getParameter("codigo");

			ArrayList<Imputacion> listado = null;

			Proyecto p = null;
			try {
				p = proyectoBD.busqueda(codigo);

				Imputacion i = new Imputacion();

				i.setProyecto(p.getId());

				listado = imputacionBD.busqueda(i, "order by fecha desc");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				destino = "/jsp/proyectosEdicion.jsp?errorBD=true";
			}

			if (destino.equals("")) {
				Date ultimaFecha = null;
				float horasImputadas = 0;

				if ((listado!=null)&&(listado.size()>0))
				{
					ultimaFecha = listado.get(0).getFecha();
					for (Imputacion imputacionTemporal : listado)
						horasImputadas += imputacionTemporal.getHoras();
				}else{
					horasImputadas=0;					
				}

				req.getSession(true).setAttribute("ultimaFecha", ultimaFecha);
				req.getSession(true).setAttribute("horasImputadas", String.valueOf(horasImputadas));
				
				req.getSession(true).setAttribute("proyecto", p);
			}
			destino = "/jsp/proyectosEdicion.jsp";

		} else if (accion.equals("modificar")) {

			String nombre = req.getParameter("nombre");
			String fechaInicio = req.getParameter("fechaInicio");
			String fechaFin = req.getParameter("fechaFin");
			String jefe = req.getParameter("jefe");
			String horas = req.getParameter("horas");
			String codigo = req.getParameter("codigo");

			fechaFin = fechaFin.replaceAll("\\/", "");
			fechaInicio = fechaInicio.replaceAll("\\/", "");

			try {

				Proyecto p = new Proyecto();
				p.setId(codigo);
				p.setFechaFin(ProyectoBD.dateFormat.parse(fechaFin));
				p.setFechaInicio(ProyectoBD.dateFormat.parse(fechaInicio));
				p.setId(codigo);
				p.setNombre(nombre);
				p.setJefe(jefe);
				p.setHoras(Float.parseFloat(horas));

				try {
					proyectoBD.actualiza(p);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					destino = "/gespro?accion=ini&errorBD=true";
				}

			} catch (ParseException e) {
				log.warning("Error al parsear fecha");
				e.printStackTrace();
				destino = "/gespro?accion=ini&errorParseo=true";
			}

			if (destino.equals(""))
				destino = "/gespro?accion=ini";

		}

		if (destino != null)
			req.getRequestDispatcher(destino).forward(req, resp);

	}

	@Override
	protected AuthorizationCodeFlow initializeFlow() throws ServletException, IOException {
		return Utils.initializeFlow();
	}

	@Override
	protected String getRedirectUri(HttpServletRequest req) throws ServletException, IOException {
		return Utils.getRedirectUri(req);
	}

}
