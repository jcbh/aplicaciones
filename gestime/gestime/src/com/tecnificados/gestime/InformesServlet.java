/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.TimeZone;

import javax.servlet.ServletException;
import javax.servlet.http.*;

import org.apache.commons.lang.StringEscapeUtils;

import com.google.api.client.auth.oauth2.AuthorizationCodeFlow;
import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.extensions.appengine.auth.oauth2.AbstractAppEngineAuthorizationCodeServlet;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.Calendar;
import com.google.api.services.calendar.model.CalendarListEntry;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.EventDateTime;
import com.google.api.services.calendar.model.Events;
import com.google.appengine.api.users.User;
import com.google.appengine.api.users.UserService;
import com.google.appengine.api.users.UserServiceFactory;
import com.google.apphosting.utils.remoteapi.RemoteApiPb.Request;
import com.tecnificados.gestime.bbdd.ImputacionBD;
import com.tecnificados.gestime.bbdd.ProyectoBD;
import com.tecnificados.gestime.bean.Imputacion;
import com.tecnificados.gestime.bean.Proyecto;

import java.util.logging.Logger;

@SuppressWarnings("serial")
public class InformesServlet extends AbstractAppEngineAuthorizationCodeServlet {

	private static final long serialVersionUID = 1L;

	private static ProyectoBD proyectoBD = new ProyectoBD();
	private static ImputacionBD imputacionBD = new ImputacionBD();

	private static final Logger log = Logger.getLogger(InformesServlet.class.getName());

	String accion = "";
	String destino = "";

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.info("Accedemos por post");
		doGet(req, resp);
	}

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

		log.info("Accedemos por get");

		accion = "";

		String destino = "";

		log.info("Accedemos por get");

		UserService userService = UserServiceFactory.getUserService();

		User u = userService.getCurrentUser();

		// Si est� autenticado
		if (req.getUserPrincipal() != null) {

			req.getSession(true).setAttribute("logOut", userService.createLogoutURL("/gestime"));
			req.getSession(true).setAttribute("userMail", req.getUserPrincipal().getName());
			req.getSession(true).setAttribute("userName", u.getNickname());

		} else {
			resp.sendRedirect(userService.createLoginURL("/gestime"));
			return;
		}

		if ((req.getParameter("accion") != null) && (!req.getParameter("accion").equals(""))) {
			accion = req.getParameter("accion");
		} else {
			accion = "ini";
		}

		log.info("La acci�n es: " + accion);

		if (accion.equals("ini")) {

			try {
				Proyecto p = new Proyecto();
				p.setJefe(u.getNickname());

				ArrayList<Proyecto> proyectos = proyectoBD.busqueda(p, "order by id");
				Map<String, String> mapaProyectoHoras = new HashMap<String, String>();

				Map<String, ArrayList<Imputacion>> mapaProyectoImputacion = new HashMap<String, ArrayList<Imputacion>>();

				Map<String, ArrayList<String>> mapaProyectoFechaImputacion = new HashMap<String, ArrayList<String>>();

				// Recorro todos los proyectos para calcular el n�mero de horas
				// imputadas
				for (Proyecto proyectoTemp : proyectos) {
					Imputacion i = new Imputacion();
					i.setProyecto(proyectoTemp.getId());
					ArrayList<Imputacion> inputacionesProyecto = imputacionBD.busqueda(i, "order by usuario");
					ArrayList<Imputacion> inputacionesProyectoFecha = imputacionBD.busqueda(i, "order by fecha");

					float horas = 0;
					for (Imputacion inputacionTemporal : inputacionesProyecto) {
						horas += inputacionTemporal.getHoras();
					}

					mapaProyectoHoras.put(proyectoTemp.getNombre() + " (" + proyectoTemp.getId() + ")",
							String.valueOf(horas) + " " + String.valueOf(proyectoTemp.getHoras()));
					mapaProyectoImputacion.put(proyectoTemp.getId(), inputacionesProyecto);

					int numImputaciones = 0;
					String fecha = "";
					if (inputacionesProyectoFecha.size() > 0)
						fecha = ImputacionBD.dateFormat.format(inputacionesProyectoFecha.get(0).getFecha());
					ArrayList<String> fechasImputaciones = new ArrayList<String>();
					for (Imputacion inputacionTemporal : inputacionesProyectoFecha) {
						if (fecha.equals(ImputacionBD.dateFormat.format(inputacionTemporal.getFecha()))) {
							numImputaciones++;
						} else {
							fechasImputaciones.add(fecha + " " + numImputaciones);
							numImputaciones = 1;
							fecha = ImputacionBD.dateFormat.format(inputacionTemporal.getFecha());
						}
					}
					fechasImputaciones.add(fecha + " " + numImputaciones);
					mapaProyectoFechaImputacion.put(proyectoTemp.getId(), fechasImputaciones);

				}

				// Introduzco en la session los proyecto que tiene este usuario
				req.getSession(true).setAttribute("proyectos", proyectos);
				// Introduzco en el mapa de imputciones y proyectos
				req.getSession(true).setAttribute("mapaProyectoImputacion", mapaProyectoImputacion);
				// Introduzco en el mapa de horas y proyectos
				req.getSession(true).setAttribute("mapa", mapaProyectoHoras);
				// Introduzco en el mapa de proyectos y fechas de imputacion con
				// imputaciones
				req.getSession(true).setAttribute("mapaProyectoFechaImputacion", mapaProyectoFechaImputacion);

			} catch (Exception e) {
				destino = "/jsp/informesProyectos.jsp?errorBD=true";
			}

			if (destino.equals("")) {
				String pId = "";
				if ((req.getParameter("pId") != null) && (!req.getParameter("pId").equals(""))) {
					pId = req.getParameter("pId");
					destino = "/jsp/imputacionesProyectos.jsp?pId=" + pId;
				} else {
					destino = "/jsp/informesProyectos.jsp";
				}
			}
		}

		if (destino != null)
			req.getRequestDispatcher(destino).forward(req, resp);

	}

	@Override
	protected AuthorizationCodeFlow initializeFlow() throws ServletException, IOException {
		return Utils.initializeFlow();
	}

	@Override
	protected String getRedirectUri(HttpServletRequest req) throws ServletException, IOException {
		return Utils.getRedirectUri(req);
	}

}
