/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime;

import javax.servlet.ServletContextListener;
import javax.servlet.ServletContextEvent;

import com.google.appengine.api.utils.SystemProperty;
import com.tecnificados.gestime.bbdd.Consulta;
import com.tecnificados.gestime.bbdd.pool;

import java.util.TimerTask;
import java.util.Timer;
import java.util.logging.Logger;

public class Configurador  implements ServletContextListener {
	
	
	private static final Logger log = Logger.getLogger(Configurador.class.getName());

	public void contextInitialized(ServletContextEvent evt) {
		
		
		System.out.println("Configurador iniciado");
		
        
        //Cargamos la configuracion de la BBDD
        
        String url="";
        
        log.info("Cargamos driver JDBC");
        try {
        	 if (SystemProperty.environment.value() == SystemProperty.Environment.Value.Production) {        		
     	        Class.forName("com.mysql.jdbc.GoogleDriver");
     	        url = "jdbc:google:mysql://tecnificados:gestime/gestime?user=root";
     	      } else {     	    	
     	        Class.forName("com.mysql.jdbc.Driver");
     	        url = "jdbc:mysql://127.0.0.1:3306/gestime";       	
     	      }
        	  log.info("Cargado.");        	  
        } catch (ClassNotFoundException e) {
        	log.warning("Driver no cargado");
            e.printStackTrace();            
        }
        
    
        log.info("Configurando driver.");
        try {
            
        	pool.setupDriver(url);
        	log.info("Configurado.");
        	
        } catch (Exception e) {
        	log.warning("Configuración erronea");        	
            e.printStackTrace();
        }
        

        //Consulta.test();        
        
        
    }

	public void contextDestroyed(ServletContextEvent evt) {
		System.out.println("ServletContextListener destroyed");
		try {
			log.info("Cerramos el driver de BBDD");
			pool.shutdownDriver();
		} catch (Exception e) {
			log.warning("El driver no se ha cerrado correctametne");
            e.printStackTrace();
		}
	}
	
	

	
}