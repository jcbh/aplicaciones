/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime.bean;

import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class EventoGC implements Comparable<EventoGC>, Serializable {

	
	private static final long serialVersionUID = 1L;
	
	private Date fechaInicial;
	private Date fechaFin;
	private String proyecto;
	private String descripcion;
	private String id;
	
	private SimpleDateFormat format;
	
	public EventoGC(){		
		super();
		format= new SimpleDateFormat("dd/MM/yyyy HH:mm");
		this.fechaInicial = new Date();
		this.fechaFin = new Date(fechaInicial.getTime() + 3600000);
		this.proyecto = "";
		this.descripcion = "";
		this.id="";
	}

	public EventoGC(Date fechaInicial, Date fechafin, String proyecto, String descripcion, String id) {
		super();
		format= new SimpleDateFormat("dd/MM/yyyy HH:mm");
		this.fechaInicial = fechaInicial;
		this.fechaFin = fechafin;
		this.proyecto = proyecto;
		this.descripcion = descripcion;
		this.id="";
	}

	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getFechaInicial() {
		return fechaInicial;
	}

	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}

	public void setFechaInicial(String fechaInicial) {	
		try {
			this.fechaInicial  = format.parse(fechaInicial);
		} catch (ParseException e) {
			this.fechaInicial=null;
			e.printStackTrace();
		}
	}

	
	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechafin) {
		this.fechaFin = fechafin;
	}
	
	public void setFechaFin(String fechafin) {		
		try {
			this.fechaFin  = format.parse(fechafin);
		} catch (ParseException e) {
			this.fechaFin=null;
			e.printStackTrace();
		}
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public boolean colisiona(EventoGC e)
	{
		
		boolean c1=(this.getFechaInicial().before(e.getFechaFin()));
		boolean c2=(this.getFechaInicial().before(e.getFechaInicial()));
		boolean c3=(this.getFechaFin().before(e.getFechaFin()));
		boolean c4=(this.getFechaFin().before(e.getFechaInicial()));
		
		boolean c5=(this.getFechaInicial().after(e.getFechaFin()));
		boolean c6=(this.getFechaInicial().after(e.getFechaInicial()));
		boolean c7=(this.getFechaFin().after(e.getFechaFin()));
		boolean c8=(this.getFechaFin().after(e.getFechaInicial()));
		
		
		
		if (c1 && c2 && c3 && c4)
			return false;
		
		if (c5 && c6 && c7 && c8)
			return false;
			
		
		
		return true;
	}
	

	public int compareTo(EventoGC e) {
		
		return this.getFechaInicial().compareTo(e.getFechaInicial());
	}
	
	public String toString(){
		return format.format(this.getFechaInicial())+"-"+format.format(this.getFechaFin())+" "+this.getProyecto()+":"+this.getDescripcion()+" id: "+this.id;
	}
	
	
	public String toJson()
	{
		return "{\"Evento\":{ \"inicio\":\"" + format.format(this.getFechaInicial()) + "\", \"fin\":\"" + format.format(this.getFechaFin()) + "\", \"proyecto\":\"" + proyecto + "\", \"descripcion\":\"" + descripcion + "\", \"id\":\"" + id + "\"}}";
		
	}
	


	public static void main(String[] args) {
		
		String dia1b="20/04/2014 09:00";
		String dia1e="20/04/2014 14:00";
		
		String dia2b="20/04/2014 15:00";
		String dia2e="20/04/2014 18:00";
		
		/*
		dia2b="20/04/2014 08:00";
		dia2e="20/04/2014 10:00";
		
		dia2b="20/04/2014 13:00";
		dia2e="20/04/2014 18:00";
		
		dia2b="20/04/2014 10:00";
		dia2e="20/04/2014 13:00";
		*/
		
		SimpleDateFormat format= new SimpleDateFormat("dd/MM/yyyy HH:mm");
		Date fechaIni= new Date();
		Date fechaFin = new Date();
		
		try {
			 fechaIni = format.parse(dia1b);
			 fechaFin = format.parse(dia1e);
		 } catch (ParseException ex) {
		     ex.printStackTrace();
		 }
		
		EventoGC ev1=new EventoGC(fechaIni,fechaFin,"P1","An�lisis","id1");
		
		try {
			 fechaIni = format.parse(dia2b);
			 fechaFin = format.parse(dia2e);
		 } catch (ParseException ex) {
		     ex.printStackTrace();
		 }
		
		EventoGC ev2=new EventoGC(fechaIni,fechaFin,"P2","Pruebas","id2");

		System.out.println(ev1.colisiona(ev2));
		
		System.out.println(ev1.toJson());
		
	}

	


}
