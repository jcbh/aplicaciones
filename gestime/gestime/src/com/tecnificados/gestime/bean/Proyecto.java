/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime.bean;

import java.io.Serializable;
import java.util.Date;



public class Proyecto implements Serializable {

	private static final long serialVersionUID = 1L;
	private String nombre;
	private float horas;
	private Date fechaInicio;
	private Date fechaFin;
	private String jefe;
	private String id;

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getHoras() {
		return horas;
	}
	
	public int getHorasI() {
		return (int)horas;
	}

	public void setHoras(float horas) {
		this.horas = horas;
	}

	public Date getFechaInicio() {
		return fechaInicio;
	}

	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}

	public Date getFechaFin() {
		return fechaFin;
	}

	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}

	public String getJefe() {
		return jefe;
	}

	public void setJefe(String jefe) {
		this.jefe = jefe;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Proyecto(String id, String nombre, float horas, Date fechaInicio, Date fechaFin, String jefe) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.horas = horas;
		this.fechaInicio = fechaInicio;
		this.fechaFin = fechaFin;
		this.jefe = jefe;
	}

	public Proyecto() {
		super();
		this.id = "";
		this.nombre = "";
		this.horas = 0;
		this.fechaInicio = null;
		this.fechaFin = null;
		this.jefe = "";
	}

	public Proyecto(Proyecto p) {
		super();
		this.id = p.getId();
		this.nombre = p.getNombre();
		this.horas = p.getHoras();
		this.fechaInicio = p.getFechaInicio();
		this.fechaFin = p.getFechaFin();
		this.jefe = p.getJefe();
	}

	@Override
	public String toString() {
		return "Proyecto [nombre=" + nombre + ", horas=" + horas + ", fechaInicio=" + fechaInicio + ", fechaFin=" + fechaFin + ", jefe=" + jefe + ", id=" + id
				+ "]";
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
