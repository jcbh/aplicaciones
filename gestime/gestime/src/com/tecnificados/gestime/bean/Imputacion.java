/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

package com.tecnificados.gestime.bean;

import java.io.Serializable;
import java.util.Date;

public class Imputacion implements Serializable {

	private static final long serialVersionUID = 1L;
	private int id;
	private String proyecto;
	private String usuario;
	private Date fecha;	
	private int estado;
	private float horas;
	private String descripcion;
	
	
	
	public Imputacion() {
		super();
		this.id = -1;
		this.proyecto = "";
		this.usuario = "";
		this.fecha = null;
		this.fecha = null;
		this.estado = -1;
		this.horas = 0;
		this.descripcion = "";
	}
	

	public Imputacion(String id, String proyecto, String usuario, Date fecha, int estado, float horas, String descripcion) {
		super();
		this.id = 1;
		this.proyecto = proyecto;
		this.usuario = usuario;
		this.fecha = fecha;		
		this.estado = estado;
		this.horas = horas;
		this.descripcion=descripcion;
	}

	
	
	public String getDescripcion() {
		return descripcion;
	}


	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public float getHoras() {
		return horas;
	}

	public void setHoras(float horas) {
		this.horas = horas;
	}
	
	
	

	@Override
	public String toString() {
		return "Imputacion [id=" + id + ", proyecto=" + proyecto + ", usuario=" + usuario + ", fecha=" + fecha + ", estado=" + estado + ", horas=" + horas
				+ ", descripcion="+descripcion+"]";
	}


	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
