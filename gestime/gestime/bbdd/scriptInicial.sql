/*
 * Copyright (c) 2014 Juan Carlos Ballesteros Hermida
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License. You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software distributed under the License
 * is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied. See the License for the specific language governing permissions and limitations under
 * the License.
 */

CREATE DATABASE gestime;
USE gestime;


CREATE TABLE `gestime`.`proyectos` (
  `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `nombre` VARCHAR(100) NOT NULL,
  `jefe` VARCHAR(100) NOT NULL,
  `fechaInicio` VARCHAR(10) NOT NULL,
  `fechaFin` VARCHAR(10) NOT NULL,
  `horas` DECIMAL(6,2) NOT NULL,
  PRIMARY KEY (`id`));

INSERT INTO proyectos (nombre,jefe,fechaInicio,FechaFin,horas) values ("_Vacaciones","Administrador","01012014","01012015",9999);  
INSERT INTO proyectos (nombre,jefe,fechaInicio,FechaFin,horas) values ("_Baja","Administrador","01012014","01012015",9999);
INSERT INTO proyectos (nombre,jefe,fechaInicio,FechaFin,horas) values ("_Trabajo no productivo","Administrador","01012014","01012015",9999);



CREATE TABLE `gestime`.`imputaciones` (
  `id` MEDIUMINT NOT NULL AUTO_INCREMENT,
  `proyecto` VARCHAR(10) NOT NULL,
  `horas` DECIMAL(6,2) NOT NULL,
  `usuario` VARCHAR(100) NOT NULL,
  `fecha` VARCHAR(10),  
  `estado` INT NOT NULL, 
  `descripcion` VARCHAR(500) NOT NULL,
  PRIMARY KEY (`id`));



commit;